"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.throwError = void 0;
function throwError() {
    console.log('before throwing error');
    throw new Error('this error was created by the throwError function');
}
exports.throwError = throwError;
//# sourceMappingURL=throwError.js.map