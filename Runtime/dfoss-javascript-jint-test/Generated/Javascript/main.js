"use strict";
const rando = new Foo.Bar.Rando();
rando.Foo();
const someGeneric = new Foo.Bar.GenericDerived();
console.log('main.ts: 1');
someGeneric.Test("foobar");
console.log('main.ts: 2');
// We're going to try to support generic methods in JINT - but for now this returns:
// No public methods with the specified arguments were found.
const foobarResult = someGeneric.FooBar(42);
console.log('main.ts: foobarResult:', foobarResult);
/*var result = someGeneric.AwesomeMethod<number>("foo", 42.69);

console.log('main.ts: 3');
console.log('result:',result);
*/
//const someGeneric2 = new Foo.Bar.MeGeneric<string>();
//console.log('main.ts: after instantiating MeGeneric<string>');
// TPC: JINT won't allow us to instantiate generic types using typescript
//const someList = new System.Collections.Generic.List<string>();
const someList = Foo.Bar.GenericFactory.InstantiateList(typeof (System.String))();
someList.Add("foo");
someList.Add("bar");
const someReadOnlyList = someList;
for (var i = 0; i < someReadOnlyList.Count; ++i) {
    console.log('look at this element:', someReadOnlyList[i]);
}
//# sourceMappingURL=main.js.map