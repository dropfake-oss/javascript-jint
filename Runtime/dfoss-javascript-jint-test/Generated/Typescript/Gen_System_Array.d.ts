// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.Array
declare namespace System {
	abstract class Array extends System.Object {
		readonly Length: number
		readonly LongLength: number
		readonly Rank: number
		readonly SyncRoot: System.Object
		readonly IsReadOnly: boolean
		readonly IsFixedSize: boolean
		readonly IsSynchronized: boolean
		static readonly MaxLength: number
		CopyTo(array: System.Array, index: number): void
		CopyTo(array: System.Array, index: number): void
		static Empty<T>(): T[]
		static Exists<T>(array: T[], match: ((obj: T) => boolean)): boolean
		static Fill<T>(array: T[], value: T): void
		static Fill<T>(array: T[], value: T, startIndex: number, count: number): void
		static Find<T>(array: T[], match: ((obj: T) => boolean)): T
		static FindAll<T>(array: T[], match: ((obj: T) => boolean)): T[]
		static FindIndex<T>(array: T[], match: ((obj: T) => boolean)): number
		static FindIndex<T>(array: T[], startIndex: number, match: ((obj: T) => boolean)): number
		static FindIndex<T>(array: T[], startIndex: number, count: number, match: ((obj: T) => boolean)): number
		static FindLast<T>(array: T[], match: ((obj: T) => boolean)): T
		static FindLastIndex<T>(array: T[], match: ((obj: T) => boolean)): number
		static FindLastIndex<T>(array: T[], startIndex: number, match: ((obj: T) => boolean)): number
		static FindLastIndex<T>(array: T[], startIndex: number, count: number, match: ((obj: T) => boolean)): number
		static ForEach<T>(array: T[], action: ((obj: T) => void)): void
		static IndexOf(array: System.Array, value?: System.Object): number
		static IndexOf(array: System.Array, value?: System.Object, startIndex: number): number
		static IndexOf(array: System.Array, value?: System.Object, startIndex: number, count: number): number
		static IndexOf<T>(array: T[], value: T): number
		static IndexOf<T>(array: T[], value: T, startIndex: number): number
		static IndexOf<T>(array: T[], value: T, startIndex: number, count: number): number
		static LastIndexOf(array: System.Array, value?: System.Object): number
		static LastIndexOf(array: System.Array, value?: System.Object, startIndex: number): number
		static LastIndexOf(array: System.Array, value?: System.Object, startIndex: number, count: number): number
		static LastIndexOf<T>(array: T[], value: T): number
		static LastIndexOf<T>(array: T[], value: T, startIndex: number): number
		static LastIndexOf<T>(array: T[], value: T, startIndex: number, count: number): number
		static Reverse(array: System.Array): void
		static Reverse(array: System.Array, index: number, length: number): void
		static Reverse<T>(array: T[]): void
		static Reverse<T>(array: T[], index: number, length: number): void
		static Sort(array: System.Array): void
		static Sort(keys: System.Array, items?: System.Array): void
		static Sort(array: System.Array, index: number, length: number): void
		static Sort(keys: System.Array, items?: System.Array, index: number, length: number): void
		static Sort(array: System.Array, comparer?: any): void
		static Sort(keys: System.Array, items?: System.Array, comparer?: any): void
		static Sort(array: System.Array, index: number, length: number, comparer?: any): void
		static Sort(keys: System.Array, items?: System.Array, index: number, length: number, comparer?: any): void
		static Sort<T>(array: T[]): void
		static Sort<TKey, TValue>(keys: TKey[], items?: TValue[]): void
		static Sort<T>(array: T[], index: number, length: number): void
		static Sort<TKey, TValue>(keys: TKey[], items?: TValue[], index: number, length: number): void
		static Sort<T>(array: T[], comparer?: any): void
		static Sort<TKey, TValue>(keys: TKey[], items?: TValue[], comparer?: any): void
		static Sort<T>(array: T[], index: number, length: number, comparer?: any): void
		static Sort<TKey, TValue>(keys: TKey[], items?: TValue[], index: number, length: number, comparer?: any): void
		static Sort<T>(array: T[], comparison: ((x: T, y: T) => number)): void
		static TrueForAll<T>(array: T[], match: ((obj: T) => boolean)): boolean
		GetEnumerator(): any
		static CreateInstance(elementType: any, length: number): System.Array
		static CreateInstance(elementType: any, length1: number, length2: number): System.Array
		static CreateInstance(elementType: any, length1: number, length2: number, length3: number): System.Array
		static CreateInstance(elementType: any, ...lengths: number[]): System.Array
		static CreateInstance(elementType: any, lengths: number[], lowerBounds: number[]): System.Array
		static Copy(sourceArray: System.Array, destinationArray: System.Array, length: number): void
		static Copy(sourceArray: System.Array, sourceIndex: number, destinationArray: System.Array, destinationIndex: number, length: number): void
		static ConstrainedCopy(sourceArray: System.Array, sourceIndex: number, destinationArray: System.Array, destinationIndex: number, length: number): void
		static Clear(array: System.Array): void
		static Clear(array: System.Array, index: number, length: number): void
		GetLength(dimension: number): number
		GetUpperBound(dimension: number): number
		GetLowerBound(dimension: number): number
		Initialize(): void
		static AsReadOnly<T>(array: T[]): any
		static Resize<T>(array?: any, newSize: number): void
		static CreateInstance(elementType: any, ...lengths: number[]): System.Array
		static Copy(sourceArray: System.Array, destinationArray: System.Array, length: number): void
		static Copy(sourceArray: System.Array, sourceIndex: number, destinationArray: System.Array, destinationIndex: number, length: number): void
		GetValue(...indices: number[]): System.Object
		GetValue(index: number): System.Object
		GetValue(index1: number, index2: number): System.Object
		GetValue(index1: number, index2: number, index3: number): System.Object
		SetValue(value?: System.Object, index: number): void
		SetValue(value?: System.Object, index1: number, index2: number): void
		SetValue(value?: System.Object, index1: number, index2: number, index3: number): void
		SetValue(value?: System.Object, ...indices: number[]): void
		GetValue(index: number): System.Object
		GetValue(index1: number, index2: number): System.Object
		GetValue(index1: number, index2: number, index3: number): System.Object
		GetValue(...indices: number[]): System.Object
		SetValue(value?: System.Object, index: number): void
		SetValue(value?: System.Object, index1: number, index2: number): void
		SetValue(value?: System.Object, index1: number, index2: number, index3: number): void
		SetValue(value?: System.Object, ...indices: number[]): void
		GetLongLength(dimension: number): number
		Clone(): System.Object
		static BinarySearch(array: System.Array, value?: System.Object): number
		static BinarySearch(array: System.Array, index: number, length: number, value?: System.Object): number
		static BinarySearch(array: System.Array, value?: System.Object, comparer?: any): number
		static BinarySearch(array: System.Array, index: number, length: number, value?: System.Object, comparer?: any): number
		static BinarySearch<T>(array: T[], value: T): number
		static BinarySearch<T>(array: T[], value: T, comparer?: any): number
		static BinarySearch<T>(array: T[], index: number, length: number, value: T): number
		static BinarySearch<T>(array: T[], index: number, length: number, value: T, comparer?: any): number
		static ConvertAll<TInput, TOutput>(array: TInput[], converter: ((input: TInput) => TOutput)): TOutput[]
	}
}
