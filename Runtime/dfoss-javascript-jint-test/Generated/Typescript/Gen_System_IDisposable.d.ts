// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.IDisposable
declare namespace System {
	interface IDisposable {
		Dispose(): void
	}
}
