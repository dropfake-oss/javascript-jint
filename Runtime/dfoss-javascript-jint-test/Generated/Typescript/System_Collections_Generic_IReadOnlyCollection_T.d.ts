// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.Collections.Generic.IReadOnlyCollection`1
declare namespace System.Collections.Generic {
	interface IReadOnlyCollection<T> {
		readonly Count: number
	}
}
