// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: dfoss-javascript-jint-test-monorepo
// Type: Foo.Bar.Rando
declare namespace Foo.Bar {
	class Rando extends System.Object {
		static Executed: boolean
		constructor()
		Foo(): void
		Bar(text?: string, rando?: Foo.Bar.Rando): void
		SomeMethod(num?: any, text: string): void
	}
}
