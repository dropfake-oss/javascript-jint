// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.Int16
declare namespace System {
	class Int16 extends System.Object {
		static readonly MaxValue: number
		static readonly MinValue: number
		CompareTo(value?: System.Object): number
		CompareTo(value: number): number
		Equals(obj?: System.Object): boolean
		Equals(obj: number): boolean
		GetHashCode(): number
		ToString(): string
		ToString(provider?: any): string
		ToString(format?: string): string
		ToString(format?: string, provider?: any): string
		TryFormat(destination: any, charsWritten: any, format: any, provider?: any): boolean
		static Parse(s: string): number
		static Parse(s: string, style: any): number
		static Parse(s: string, provider?: any): number
		static Parse(s: string, style: any, provider?: any): number
		static Parse(s: any, style: any, provider?: any): number
		static TryParse(s?: string, result?: any): boolean
		static TryParse(s: any, result: any): boolean
		static TryParse(s?: string, style: any, provider?: any, result?: any): boolean
		static TryParse(s: any, style: any, provider?: any, result: any): boolean
		GetTypeCode(): any
	}
}
