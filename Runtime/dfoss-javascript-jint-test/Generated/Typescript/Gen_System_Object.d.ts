// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.Object
declare namespace System {
	class Object {
		constructor()
		GetType(): any
		ToString(): string
		Equals(obj?: System.Object): boolean
		static Equals(objA?: System.Object, objB?: System.Object): boolean
		static ReferenceEquals(objA?: System.Object, objB?: System.Object): boolean
		GetHashCode(): number
	}
}
