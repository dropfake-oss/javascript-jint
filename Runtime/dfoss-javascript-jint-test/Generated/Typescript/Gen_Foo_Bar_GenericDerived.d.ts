// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: dfoss-javascript-jint-test-monorepo
// Type: Foo.Bar.GenericDerived
declare namespace Foo.Bar {
	class GenericDerived extends Foo.Bar.MeGeneric<string> {
		constructor()
		Test(bar: string): void
		FooBar<V>(v: V): V
	}
}
