// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.String
declare namespace System {
	class String extends System.Object {
		static readonly Empty: string
		readonly Chars: string
		readonly Length: number
		constructor(value?: string[])
		constructor(value: string[], startIndex: number, length: number)
		constructor(value: any)
		constructor(value: any, startIndex: number, length: number)
		constructor(value: any)
		constructor(value: any, startIndex: number, length: number)
		constructor(value: any, startIndex: number, length: number, enc: any)
		constructor(c: string, count: number)
		constructor(value: any)
		LastIndexOf(value: string, startIndex: number, count: number): number
		LastIndexOf(value: string, comparisonType: any): number
		LastIndexOf(value: string, startIndex: number, comparisonType: any): number
		LastIndexOf(value: string, startIndex: number, count: number, comparisonType: any): number
		PadRight(totalWidth: number): string
		PadRight(totalWidth: number, paddingChar: string): string
		Remove(startIndex: number, count: number): string
		Remove(startIndex: number): string
		Replace(oldValue: string, newValue?: string, ignoreCase: boolean, culture?: any): string
		Replace(oldValue: string, newValue?: string, comparisonType: any): string
		Replace(oldChar: string, newChar: string): string
		Replace(oldValue: string, newValue?: string): string
		ReplaceLineEndings(): string
		ReplaceLineEndings(replacementText: string): string
		Split(separator: string, options: any): string[]
		Split(separator: string, count: number, options: any): string[]
		Split(...separator: string[]): string[]
		Split(separator?: string[], count: number): string[]
		Split(separator?: string[], options: any): string[]
		Split(separator?: string[], count: number, options: any): string[]
		Split(separator?: string, options: any): string[]
		Split(separator?: string, count: number, options: any): string[]
		Split(separator?: string[], options: any): string[]
		Split(separator?: string[], count: number, options: any): string[]
		Substring(startIndex: number): string
		Substring(startIndex: number, length: number): string
		ToLower(): string
		ToLower(culture?: any): string
		ToLowerInvariant(): string
		ToUpper(): string
		ToUpper(culture?: any): string
		ToUpperInvariant(): string
		Trim(): string
		Trim(trimChar: string): string
		Trim(...trimChars: string[]): string
		TrimStart(): string
		TrimStart(trimChar: string): string
		TrimStart(...trimChars: string[]): string
		TrimEnd(): string
		TrimEnd(trimChar: string): string
		TrimEnd(...trimChars: string[]): string
		Contains(value: string): boolean
		Contains(value: string, comparisonType: any): boolean
		Contains(value: string): boolean
		Contains(value: string, comparisonType: any): boolean
		IndexOf(value: string): number
		IndexOf(value: string, startIndex: number): number
		IndexOf(value: string, comparisonType: any): number
		IndexOf(value: string, startIndex: number, count: number): number
		IndexOfAny(anyOf: string[]): number
		IndexOfAny(anyOf: string[], startIndex: number): number
		IndexOfAny(anyOf: string[], startIndex: number, count: number): number
		IndexOf(value: string): number
		IndexOf(value: string, startIndex: number): number
		IndexOf(value: string, startIndex: number, count: number): number
		IndexOf(value: string, comparisonType: any): number
		IndexOf(value: string, startIndex: number, comparisonType: any): number
		IndexOf(value: string, startIndex: number, count: number, comparisonType: any): number
		LastIndexOf(value: string): number
		LastIndexOf(value: string, startIndex: number): number
		LastIndexOf(value: string, startIndex: number, count: number): number
		LastIndexOfAny(anyOf: string[]): number
		LastIndexOfAny(anyOf: string[], startIndex: number): number
		LastIndexOfAny(anyOf: string[], startIndex: number, count: number): number
		LastIndexOf(value: string): number
		LastIndexOf(value: string, startIndex: number): number
		static IsNullOrEmpty(value?: string): boolean
		static IsNullOrWhiteSpace(value?: string): boolean
		GetPinnableReference(): any
		ToString(): string
		ToString(provider?: any): string
		GetEnumerator(): any
		EnumerateRunes(): any
		GetTypeCode(): any
		IsNormalized(): boolean
		IsNormalized(normalizationForm: any): boolean
		Normalize(): string
		Normalize(normalizationForm: any): string
		static Concat(arg0?: System.Object): string
		static Concat(arg0?: System.Object, arg1?: System.Object): string
		static Concat(arg0?: System.Object, arg1?: System.Object, arg2?: System.Object): string
		static Concat(...args: System.Object[]): string
		static Concat<T>(values: any): string
		static Concat(values: any): string
		static Concat(str0?: string, str1?: string): string
		static Concat(str0?: string, str1?: string, str2?: string): string
		static Concat(str0?: string, str1?: string, str2?: string, str3?: string): string
		static Concat(str0: any, str1: any): string
		static Concat(str0: any, str1: any, str2: any): string
		static Concat(str0: any, str1: any, str2: any, str3: any): string
		static Concat(...values: string[]): string
		static Format(format: string, arg0?: System.Object): string
		static Format(format: string, arg0?: System.Object, arg1?: System.Object): string
		static Format(format: string, arg0?: System.Object, arg1?: System.Object, arg2?: System.Object): string
		static Format(format: string, ...args: System.Object[]): string
		static Format(provider?: any, format: string, arg0?: System.Object): string
		static Format(provider?: any, format: string, arg0?: System.Object, arg1?: System.Object): string
		static Format(provider?: any, format: string, arg0?: System.Object, arg1?: System.Object, arg2?: System.Object): string
		static Format(provider?: any, format: string, ...args: System.Object[]): string
		Insert(startIndex: number, value: string): string
		static Join(separator: string, ...value: string[]): string
		static Join(separator?: string, ...value: string[]): string
		static Join(separator: string, value: string[], startIndex: number, count: number): string
		static Join(separator?: string, value: string[], startIndex: number, count: number): string
		static Join(separator?: string, values: any): string
		static Join(separator: string, ...values: System.Object[]): string
		static Join(separator?: string, ...values: System.Object[]): string
		static Join<T>(separator: string, values: any): string
		static Join<T>(separator?: string, values: any): string
		PadLeft(totalWidth: number): string
		PadLeft(totalWidth: number, paddingChar: string): string
		static Intern(str: string): string
		static IsInterned(str: string): string
		static Compare(strA?: string, strB?: string): number
		static Compare(strA?: string, strB?: string, ignoreCase: boolean): number
		static Compare(strA?: string, strB?: string, comparisonType: any): number
		static Compare(strA?: string, strB?: string, culture?: any, options: any): number
		static Compare(strA?: string, strB?: string, ignoreCase: boolean, culture?: any): number
		static Compare(strA?: string, indexA: number, strB?: string, indexB: number, length: number): number
		static Compare(strA?: string, indexA: number, strB?: string, indexB: number, length: number, ignoreCase: boolean): number
		static Compare(strA?: string, indexA: number, strB?: string, indexB: number, length: number, ignoreCase: boolean, culture?: any): number
		static Compare(strA?: string, indexA: number, strB?: string, indexB: number, length: number, culture?: any, options: any): number
		static Compare(strA?: string, indexA: number, strB?: string, indexB: number, length: number, comparisonType: any): number
		static CompareOrdinal(strA?: string, strB?: string): number
		static CompareOrdinal(strA?: string, indexA: number, strB?: string, indexB: number, length: number): number
		CompareTo(value?: System.Object): number
		CompareTo(strB?: string): number
		EndsWith(value: string): boolean
		EndsWith(value: string, comparisonType: any): boolean
		EndsWith(value: string, ignoreCase: boolean, culture?: any): boolean
		EndsWith(value: string): boolean
		Equals(obj?: System.Object): boolean
		Equals(value?: string): boolean
		Equals(value?: string, comparisonType: any): boolean
		static Equals(a?: string, b?: string): boolean
		static Equals(a?: string, b?: string, comparisonType: any): boolean
		GetHashCode(): number
		GetHashCode(comparisonType: any): number
		static GetHashCode(value: any): number
		static GetHashCode(value: any, comparisonType: any): number
		StartsWith(value: string): boolean
		StartsWith(value: string, comparisonType: any): boolean
		StartsWith(value: string, ignoreCase: boolean, culture?: any): boolean
		StartsWith(value: string): boolean
		static Create<TState>(length: number, state: TState, action: ((span: any, arg: TState) => void)): string
		static Create(provider?: any, handler: any): string
		static Create(provider?: any, initialBuffer: any, handler: any): string
		Clone(): System.Object
		static Copy(str: string): string
		CopyTo(sourceIndex: number, destination: string[], destinationIndex: number, count: number): void
		CopyTo(destination: any): void
		TryCopyTo(destination: any): boolean
		ToCharArray(): string[]
		ToCharArray(startIndex: number, length: number): string[]
		AsSpan(): any
		AsSpan(start: number): any
		AsSpan(start: number, length: number): any
		AsMemory(): any
		AsMemory(start: number): any
		AsMemory(startIndex: any): any
		AsMemory(start: number, length: number): any
		AsMemory(range: any): any
		IsNormalized(): boolean
		IsNormalized(normalizationForm: any): boolean
		Normalize(): string
		Normalize(normalizationForm: any): string
		AddDoubleQuote(): string
		FormatWith(provider: any, arg0: System.Object): string
		FormatWith(provider: any, arg0: System.Object, arg1: System.Object): string
		FormatWith(provider: any, arg0: System.Object, arg1: System.Object, arg2: System.Object): string
		FormatWith(provider: any, arg0: System.Object, arg1: System.Object, arg2: System.Object, arg3: System.Object): string
		StartsWith(value: string): boolean
		EndsWith(value: string): boolean
		ToXml(): any
		IsNullOrWhiteSpace(): boolean
		ParseRuntime(): any
		CharCodeAt(index: number): string
	}
}
