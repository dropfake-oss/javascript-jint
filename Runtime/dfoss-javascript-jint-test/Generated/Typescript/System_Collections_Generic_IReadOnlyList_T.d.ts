// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.Collections.Generic.IReadOnlyList`1
declare namespace System.Collections.Generic {
	interface IReadOnlyList<T> extends System.Collections.Generic.IReadOnlyCollection<T> {
		[index: number]: T;
	}
}
