// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: dfoss-javascript-jint-test-monorepo
// Type: Foo.Bar.Rando+NestedEnum
declare namespace Foo.Bar.Rando {
	enum NestedEnum {
		Foo = 0,
		Bar = 1,
		AnotherBar = 2,
		SomeEntry = 3,
	}
}
