// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.IObservable`1
declare namespace System {
	interface IObservable<T> {
		Subscribe(observer: System.IObserver<T>): System.IDisposable
	}
}
