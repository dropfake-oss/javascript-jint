// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: dfoss-javascript-jint-test-monorepo
// Type: Foo.Bar.MeGeneric`1
declare namespace Foo.Bar {
	class MeGeneric<T> extends System.Object {
		constructor()
		AwesomeMethod<U>(t: T, u: U): U
	}
}
