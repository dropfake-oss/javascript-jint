// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.Double
declare namespace System {
	class Double extends System.Object {
		static readonly MinValue: number
		static readonly MaxValue: number
		static readonly Epsilon: number
		static readonly NegativeInfinity: number
		static readonly PositiveInfinity: number
		static readonly NaN: number
		static IsFinite(d: number): boolean
		static IsInfinity(d: number): boolean
		static IsNaN(d: number): boolean
		static IsNegative(d: number): boolean
		static IsNegativeInfinity(d: number): boolean
		static IsNormal(d: number): boolean
		static IsPositiveInfinity(d: number): boolean
		static IsSubnormal(d: number): boolean
		CompareTo(value?: System.Object): number
		CompareTo(value: number): number
		Equals(obj?: System.Object): boolean
		Equals(obj: number): boolean
		GetHashCode(): number
		ToString(): string
		ToString(format?: string): string
		ToString(provider?: any): string
		ToString(format?: string, provider?: any): string
		TryFormat(destination: any, charsWritten: any, format: any, provider?: any): boolean
		static Parse(s: string): number
		static Parse(s: string, style: any): number
		static Parse(s: string, provider?: any): number
		static Parse(s: string, style: any, provider?: any): number
		static Parse(s: any, style: any, provider?: any): number
		static TryParse(s?: string, result?: any): boolean
		static TryParse(s: any, result: any): boolean
		static TryParse(s?: string, style: any, provider?: any, result?: any): boolean
		static TryParse(s: any, style: any, provider?: any, result: any): boolean
		GetTypeCode(): any
		ToDateTime(): any
		AsNumberOfType(type: any): System.Object
	}
}
