// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: System.Private.CoreLib
// Type: System.Exception
declare namespace System {
	class Exception extends System.Object {
		readonly TargetSite: any
		readonly Message: string
		readonly Data: any
		readonly InnerException: System.Exception
		HelpLink: string
		Source: string
		HResult: number
		readonly StackTrace: string
		constructor()
		constructor(message?: string)
		constructor(message?: string, innerException?: System.Exception)
		GetBaseException(): System.Exception
		GetObjectData(info: any, context: any): void
		ToString(): string
		GetType(): any
		GetStackTraceWithoutThrowing(): string
		GetMessageWithoutThrowing(): string
		GetDataWithoutThrowing(): any
	}
}
