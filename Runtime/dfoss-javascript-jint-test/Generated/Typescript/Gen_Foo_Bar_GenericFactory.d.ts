// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: dfoss-javascript-jint-test-monorepo
// Type: Foo.Bar.GenericFactory
declare namespace Foo.Bar {
	class GenericFactory extends System.Object {
		constructor()
		static InstantiateList<T>(t: T): (() => System.Collections.Generic.List<T>)
	}
}
