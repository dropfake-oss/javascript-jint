// Copyright 2022-present Drop Fake Inc. All rights reserved.
// Assembly: dfoss-javascript-jint-test-monorepo
// Type: Foo.Bar.ThisUsesGenericList
declare namespace Foo.Bar {
	class ThisUsesGenericList extends System.Object {
		Strings: System.Collections.Generic.List<string>
		constructor()
	}
}
