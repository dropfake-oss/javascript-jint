import { Animal } from '../animal';

export class Monkey extends Animal {
	constructor(name:string) {
		super('Monkey', name);
	}
}
