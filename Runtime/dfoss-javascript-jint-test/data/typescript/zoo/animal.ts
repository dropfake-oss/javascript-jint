export class Animal {
	private readonly AnimalType: string;
	private readonly Name: string;

	constructor(animalType: string, name:string) {
		this.AnimalType = animalType;
		this.Name = name;
	}

	print() {
		console.log(this.AnimalType, 'name is:', this.Name);
	}
}
