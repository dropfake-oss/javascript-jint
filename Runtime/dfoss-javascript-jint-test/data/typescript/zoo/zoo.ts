import { Panda } from "./animals/panda";
import { Monkey } from "./animals/monkey";

let panda = new Panda('Da Mao');
panda.print();

let monkey = new Monkey('Rafiki');
monkey.print();

let animals = [panda,monkey];
export = animals.length;
