"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        console.log('d:', d, 'typeof d:', (typeof d));
        console.log('b:', b, 'typeof b:', (typeof b));
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
//exports.createHQLoseCondition = exports.TreeCondition = exports.AggregateCondition = exports.EntityDeathCondition = exports.Condition = void 0;

var eventemitter_1 = require("./eventemitter");
console.log('eventemitter_1:', eventemitter_1, 'typeof eventemitter_1:', (typeof eventemitter_1));
console.log('eventemitter_1.EventEmitter:', eventemitter_1.EventEmitter);
var Condition = /** @class */ (function (_super) {
    __extends(Condition, _super);
    function Condition() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Condition.prototype.done = function (won, msg, display) {
        if (display) {
            Scripting.UI.AddToast(won ? 'Objective Complete: ' : 'Objective Failed: ' + msg);
        }
        if (!this.result) {
            this.result = { won: won, msg: msg };
            this.emit(this.result);
        }
    };
    return Condition;
}(eventemitter_1.EventEmitter));

exports.Condition = Condition;

