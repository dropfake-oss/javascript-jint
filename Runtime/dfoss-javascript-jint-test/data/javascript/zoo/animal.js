"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Animal = void 0;
class Animal {
    constructor(animalType, name) {
        this.AnimalType = animalType;
        this.Name = name;
    }
    print() {
        console.log(this.AnimalType, 'name is:', this.Name);
    }
}
exports.Animal = Animal;
//# sourceMappingURL=animal.js.map