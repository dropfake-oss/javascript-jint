"use strict";
const panda_1 = require("./animals/panda");
const monkey_1 = require("./animals/monkey");
let panda = new panda_1.Panda('Da Mao');
panda.print();
let monkey = new monkey_1.Monkey('Rafiki');
monkey.print();
let animals = [panda, monkey];
module.exports = animals.length;
//# sourceMappingURL=zoo.js.map