"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Panda = void 0;
const animal_1 = require("../animal");
class Panda extends animal_1.Animal {
    constructor(name) {
        super('Panda', name);
    }
}
exports.Panda = Panda;
//# sourceMappingURL=panda.js.map