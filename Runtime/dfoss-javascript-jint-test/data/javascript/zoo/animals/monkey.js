"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Monkey = void 0;
const animal_1 = require("../animal");
class Monkey extends animal_1.Animal {
    constructor(name) {
        super('Monkey', name);
    }
}
exports.Monkey = Monkey;
//# sourceMappingURL=monkey.js.map