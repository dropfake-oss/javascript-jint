"use strict";
Object.defineProperty(exports, "__esModule", { value: true });


var funcReturnsFunction = function () {
	return function (a, b) {
		console.log('a:', a);
		console.log('b:', b);
	};
}();

var testObj = { foo: 'bar' };
var num = 42;
funcReturnsFunction(testObj, num);
console.log('called function');
