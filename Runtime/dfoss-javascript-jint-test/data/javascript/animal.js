

class Animal {
    constructor(name) {
        this.name = name;
    }

    print() {
        console.log('animal name is:' + this.name);
    }
}

//module.exports = Animal;
exports.Animal = Animal;

