using Jint;
using Jint.Runtime.Interop;
using Jint.Runtime;
using System;
using NUnit.Framework;
using System.Threading.Tasks;
using System.Collections.Generic;
using Javascript.Core;
using Javascript.Jint;

namespace Foo.Bar
{
	class Tester
	{
		public static bool MethodCalled
		{
			get;
			private set;
		}

		public void Log()
		{
			MethodCalled = true;
			Console.WriteLine("Foo.Bar: Tester: Log");
		}
	}

	class JintTester
	{
		public static bool MethodCalled
		{
			get;
			private set;
		}

		public void Write()
		{
			MethodCalled = true;
			Console.WriteLine("Foo.Bar: JintTester: Write");
		}
	}
}

public class JintLoadScriptFromFileTests
{
	private Javascript.Jint.JintVirtualMachine _jintVirtualMachine;

	private IJavascriptLogger _logger;

	public JintLoadScriptFromFileTests()
	{
		_logger = new DefaultLogger();
		var jintFileSystem = new Javasscript.Jint.Tests.JintTestFileSystem();
		var assembly = typeof(Foo.Bar.Tester).Assembly;
		var assemblyNamespaces = new AssemblyNamespaces(assembly, new string[] { "Foo" });
		var assemblyNamespacesArray = new AssemblyNamespaces[] { assemblyNamespaces };
		_jintVirtualMachine = new JintVirtualMachine(_logger, jintFileSystem, assemblyNamespacesArray);
	}

	[OneTimeTearDown]
	protected void TearDown()
	{
	}

	private System.IO.DirectoryInfo GetJavascriptDataDirInfo()
	{
		var currentDir = System.IO.Directory.GetCurrentDirectory();
		// /Users/tcassidy/gitlab/javascript-jint/Runtime/dfoss-javascript-jint-test/bin/Debug/net5.0
		Console.WriteLine("JavascriptRequire: currentDir: " + currentDir);

		System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(currentDir);

		var dataDirInfos = dirInfo.GetDirectories("data");
		Assert.AreEqual(1, dataDirInfos.Length);

		var dataDirInfo = dataDirInfos[0];
		var javascriptDirInfos = dataDirInfo.GetDirectories("javascript");
		Assert.AreEqual(1, javascriptDirInfos.Length);

		var javascriptDirInfo = javascriptDirInfos[0];
		return javascriptDirInfo;
	}

	[Test()]
	public void LoadAndRunTrivialJavascriptFile()
	{
		var jsDir = "./data/javascript";
		var jsFilePath = System.IO.Path.Combine(jsDir, "simple");

		var jsValue = _jintVirtualMachine.Execute(jsFilePath);
		Console.WriteLine("LoadAndRunTrivialJavascriptFile: jsValue: " + jsValue);
	}

	[Test()]
	public void FunctionReturnsFunction()
	{
		var jsDir = "./data/javascript";
		var jsFilePath = System.IO.Path.Combine(jsDir, "functionReturnsFunction");

		var jsValue = _jintVirtualMachine.Execute(jsFilePath);
		Console.WriteLine("jsValue: " + jsValue);
	}

	[Test()]
	public void SetGlobalClassAndReferenceFromJavascriptFile()
	{
		Assert.AreEqual(false, Foo.Bar.JintTester.MethodCalled);




		// TPC: TODO: load some classes from a namespace into global scope
		//_jintVirtualMachine.EvalInMem("inMem.global", @"
		_jintVirtualMachine.EvalInMem("inMem.js", @"
			//const Foo = {};
			var Foo = {};
			Foo.Bar = importNamespace('Foo.Bar');

			var jintTester = new Foo.Bar.JintTester();
			jintTester.Write();
		");

		Assert.AreEqual(true, Foo.Bar.JintTester.MethodCalled);
		Assert.AreEqual(false, Foo.Bar.Tester.MethodCalled);

		var jsDir = "./data/javascript";
		var jsFilePath = System.IO.Path.Combine(jsDir, "cat");

		var jsValue = _jintVirtualMachine.Execute(jsFilePath);
		Console.WriteLine("LoadAndRunTrivialJavascriptFile: jsValue: " + jsValue);
		Assert.AreEqual(true, Foo.Bar.Tester.MethodCalled);
	}

}
