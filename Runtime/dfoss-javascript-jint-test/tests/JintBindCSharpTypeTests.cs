using Jint.Runtime.Interop;
using Jint.Runtime;
using System;
using NUnit.Framework;
using Javascript.Jint;
using Javascript.Core;

public class Rando
{
	public void Foo()
	{
		Console.WriteLine("Rando::Foo()");
	}
}

[TestFixture()]
public class JintBindCSharpTypeTests
{
	private JintVirtualMachine _jintVm;
	private IJavascriptLogger _logger;

	public JintBindCSharpTypeTests()
	{
		_logger = new DefaultLogger();
		var jintFileSystem = new Javascript.Core.InMemoryFileSystem();
		//var testAssembly = (typeof( Rando)).Assembly;
		//_jintVm = new JintVirtualMachine(_logger, jintFileSystem, testAssembly);
		_jintVm = new JintVirtualMachine(_logger, jintFileSystem);
	}

	[Test()]
	public void JavascriptCallsCSharpMethod()
	{
		//_jintVm.EvalInMem("someFile.js", "{" + Environment.NewLine + script + Environment.NewLine + "}");
		_jintVm.SetValue("Rando", typeof(Rando));
		string script = $@"
			console.log('JintBindCSharpTypeTests: JavascriptCallsCSharpMethod: 2');

			var rando = new Rando();
			rando.Foo();
			console.log('JintBindCSharpTypeTests: JavascriptCallsCSharpMethod: after rando');
		";

		// TPC: adding scope block here - since we declare/define counter in both tests (at global level)
		_jintVm.EvalInMem("someFile.js", "{" + Environment.NewLine + script + Environment.NewLine + "}");
	}



}
