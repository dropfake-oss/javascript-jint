using System;
using System.Collections.Generic;
using Javascript.Codegen.Dts;
using System.IO;

namespace Javascript.Tests
{
	public class DotNetToTypescriptDeclaration
	{
		private static string RootFilePath = "";
		public static string typescriptDir = "Generated/Typescript";

		private static List<Type> _registerTypes = new List<Type>();
		public static IReadOnlyList<Type> RegisteredTypes
		{
			get { return _registerTypes; }
		}

		public static void RegisterType(Type type)
		{
			_registerTypes.Add(type);
		}

		private static void WriteAllText(string path, string contents)
		{
			File.WriteAllText(path, contents);
		}

		private static void MapDotNetTypesToTypescriptTypes(TypeTranslator typeTranslator)
		{
			//typeTranslator.AddTSTypeNameMap(typeof(<some type>), "<some type name>");
		}

		private static void AddExportedTypes(TypescriptDefinitionGeneration typescriptDefinitionGeneration)
		{
			foreach(var type in RegisteredTypes)
			{
				typescriptDefinitionGeneration.RegisterType(type);
			}
		}

		private static void WriteTypescriptDeclarationFile(TypescriptDeclarationGenerator generatedType)
		{
			var filename = generatedType.CodeGenType.GetFileName();
			var tsName = filename + ".d.ts";
			var tsPath = Path.Combine(RootFilePath, typescriptDir, tsName);

			//Console.WriteLine("WriteTypescriptDeclarationFile: path: " + tsPath + " contents: " + generatedType.ToString());
			WriteAllText(tsPath, generatedType.ToString());
		}

		public static void SetRootFilePath(string path)
		{
			RootFilePath = path;
		}

		public static void GenerateTypescriptDefinitions(IReadOnlyList<System.Reflection.Assembly>? codeGenEntireAssemblies = null)
		{
			Console.WriteLine("GenerateTypescriptDefinitions");
			string fullPath = Path.Combine(RootFilePath, typescriptDir);
			if (!Directory.Exists(fullPath))
			{
				Directory.CreateDirectory(fullPath);
			}

			ConsoleCodeGenLogger codegenLogger = new ConsoleCodeGenLogger();
			TypeTranslator typeTranslator = new TypeTranslator(codegenLogger);
			//MapDotNetTypesToTypescriptTypes(typeTranslator);
			TypescriptDefinitionGeneration typescriptDefinitionGeneration = new TypescriptDefinitionGeneration(codegenLogger, typeTranslator, WriteTypescriptDeclarationFile);
			AddExportedTypes(typescriptDefinitionGeneration);

			// We want to add all assemblies to our CodeGen - so it can resolve types that exist external to the assembly of the type we're code-gen'ing (example partial classes or extension methods)
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			List<CodeGenAssembly> codeGenAssemblies = new List<CodeGenAssembly>();
			for (var i = 0; i < assemblies.Length; i++)
			{
				var assembly = assemblies[i];
				//Debug.Log("GenerateTypescriptDefinitions: assembly: " + assembly.FullName + " IsDynamic: " + assembly.IsDynamic);
				if (assembly.IsDynamic)
				{
					continue;
				}
				var codegenEntireAssembly = false;
				if (codeGenEntireAssemblies != null)
				{
					foreach (var codegenAssembly in codeGenEntireAssemblies)
					{
						if (codegenAssembly == assembly)
						{
							codegenEntireAssembly = true;
							break;
						}
					}
				}
				codeGenAssemblies.Add(new CodeGenAssembly(assembly, codegenEntireAssembly));
			}

			typescriptDefinitionGeneration.GenerateTypescriptDeclarations(codeGenAssemblies.ToArray());
		}
	}
}
