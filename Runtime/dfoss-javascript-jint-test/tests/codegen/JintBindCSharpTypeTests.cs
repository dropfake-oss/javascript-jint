using Jint.Runtime.Interop;
using Jint.Runtime;
using System;
using NUnit.Framework;
using Javascript.Jint;
using Javascript.Core;

using System.Reflection;
using System.Collections.Generic;
using System.IO;

namespace Foo.Bar
{
	public class GenericFactory
	{
		public static Func<List<T>> InstantiateList<T>(T t)
		{
			return () => new List<T>();
		}
	}

	public class Rando
	{
		public enum NestedEnum
		{
			Foo,
			Bar,
			AnotherBar,
			SomeEntry
		}

		public static bool Executed = false;
		public void Foo()
		{
			Console.WriteLine("Rando::Foo()");
			Executed = true;
		}

		public void Bar(string? text, Rando? rando)
		{
			// this is a test of nullable parameters
			Console.WriteLine("Rando: Bar: text: " + text);
		}

		public void SomeMethod(int? num, string text)
		{
			Console.WriteLine("Rando: SomeMethod: num: " + num);
		}
	}

	public class MeGeneric<T>
	{
		public U AwesomeMethod<U>(T t, U u)
		{
			Console.WriteLine("MeGeneric: AwesomeMethod: t: " + t);
			return u;
		}
	}

	public class GenericDerived : MeGeneric<string>
	{
		public void Test(string bar)
		{
			Console.WriteLine("GenericDerived: Test: bar: " + bar);
		}

		public V FooBar<V>(V v)
		{
			Console.WriteLine("GenericDerived: FooBar: v: " + v);
			return v;
		}
	}

	public class ThisUsesGenericList
	{
		public List<string> Strings;
	}
}

public class TestFileSystem : IFileSystem
{
	public bool Exists(string path)
	{
		Console.WriteLine("TestFileSystem: Exists: path: " + path + " Environment.CurrentDirectory: " + Environment.CurrentDirectory);
		return System.IO.File.Exists(path);
	}

	public byte[] ReadAllBytes(string path)
	{
		try
		{
			return System.IO.File.ReadAllBytes(path);
		}
		catch
		{
			//return null;
			// TPC: not sure what benefit there could be in returning null for a file that isn't found
			throw;
		}
	}

	public void WriteFile(string path, string contents)
	{
		//System.IO.File.WriteAllText(path, contents);
		//Console.WriteLine("TestFileSystem: write to path: " + path + " contents: " + contents);
		throw new System.NotImplementedException("TestFileSystem: WriteFile: path: " + path + " contents: " + contents.Length);
	}

	public void WriteFile(string path, byte[] bytes)
	{
		//System.IO.File.WriteAllBytes(path, bytes);
		//var contents = System.Text.Encoding.Default.GetString(bytes);
		//Console.WriteLine("TestFileSystem: write to path: " + path + " contents: " + contents);
		throw new System.NotImplementedException("TestFileSystem: WriteFile: path: " + path + " contents: " + bytes.Length);
	}
}



[TestFixture()]
public class JintCodeGenTests
{
	private JintVirtualMachine _jintVm;
	private IJavascriptLogger _logger;

	public JintCodeGenTests()
	{
	}

	public static IReadOnlyList<AssemblyNamespaces> GetAssemblyNamespaces()
	{
		var executingAssembly = Assembly.GetExecutingAssembly();
		var executingAssemblyNamespaces = new AssemblyNamespaces(executingAssembly, new string[] { "Foo" });

		var assemblyNamespaces = new AssemblyNamespaces[] { executingAssemblyNamespaces };

		return assemblyNamespaces;
	}

	static IReadOnlyList<Assembly> GetAssemblies()
	{
		List<Assembly> assemblies = new List<Assembly>();
		var assemblyNamespacesList = GetAssemblyNamespaces();
		foreach (var assemblyNamespaces in assemblyNamespacesList)
		{
			assemblies.Add(assemblyNamespaces.Assembly);
		}
		return assemblies;
	}

	private static void RegisterObservableClasses()
	{
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(System.IDisposable));
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(System.Exception));
		// IObservable has dependency on IObserver - so we need to codegen IObserver in order to codegen IObservable
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(System.IObserver<>));
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(System.IObservable<>));
	}

	public string GetCsProjDirectory()
	{
		string workingDirectory = Environment.CurrentDirectory;
		string projectDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName;
		return projectDirectory;
	}

	private void GenerateCode(string rootFilePath)
	{
		Javascript.Tests.DotNetToTypescriptDeclaration.SetRootFilePath(rootFilePath);

		RegisterObservableClasses();
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(Foo.Bar.Rando));
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(Foo.Bar.Rando.NestedEnum));
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(Foo.Bar.MeGeneric<>));
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(Foo.Bar.GenericDerived));
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(Foo.Bar.GenericFactory));

		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(System.Collections.Generic.List<>));
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(Foo.Bar.ThisUsesGenericList));

		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(IReadOnlyCollection<>));
		Javascript.Tests.DotNetToTypescriptDeclaration.RegisterType(typeof(IReadOnlyList<>));

		Javascript.Tests.DotNetToTypescriptDeclaration.GenerateTypescriptDefinitions();
	}

	private void InitJavascriptVm()
	{
		_logger = new DefaultLogger();
		var jintTestFileSystem = new TestFileSystem();
		var assemblyNamespaces = GetAssemblyNamespaces();
		_jintVm = new JintVirtualMachine(_logger, jintTestFileSystem, assemblyNamespaces);
	}

	[Test()]
	public void GenerateCodeAndExecuteJavascriptTest()
	{
		Console.WriteLine("GenerateCodeTest");
		var rootFilePath = GetCsProjDirectory();

		GenerateCode(rootFilePath);

		InitJavascriptVm();

		_jintVm.EvalInMem("file.js", @"
			console.log('does this work?');
		");

		var mainJsFilename = "main.js";
		var mainJsFile = Path.Combine(rootFilePath, "Generated", "Javascript", mainJsFilename);
		// TPC: we're cheating a little here - since the js file we're executing is committed to git - i.e. we're not regen'ing it unless someone runs "tsc"
		_jintVm.Execute(mainJsFile);

		Assert.AreEqual(true, Foo.Bar.Rando.Executed);
	}
}
