using Jint;
using Jint.Runtime.Interop;
using Jint.Runtime;
using System;
using NUnit.Framework;
using System.Threading.Tasks;
using Javascript.Core;

public class JintRequireTests
{
	private Javascript.Jint.JintVirtualMachine _jintVirtualMachine;

	private IJavascriptLogger _logger;

	public JintRequireTests()
	{
		_logger = new DefaultLogger();
		var jintFileSystem = new Javasscript.Jint.Tests.JintTestFileSystem();
		_jintVirtualMachine = new Javascript.Jint.JintVirtualMachine(_logger, jintFileSystem);
	}

	[OneTimeTearDown]
	protected void TearDown()
	{
	}

	private System.IO.DirectoryInfo GetJavascriptDataDirInfo()
	{
		var currentDir = System.IO.Directory.GetCurrentDirectory();

		System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(currentDir);

		var dataDirInfos = dirInfo.GetDirectories("data");
		Assert.AreEqual(1, dataDirInfos.Length);

		var dataDirInfo = dataDirInfos[0];
		var javascriptDirInfos = dataDirInfo.GetDirectories("javascript");
		Assert.AreEqual(1, javascriptDirInfos.Length);

		var javascriptDirInfo = javascriptDirInfos[0];
		return javascriptDirInfo;
	}

	[Test()]
	public void JavascriptClassRequire()
	{
		var zooDir = "./data/javascript/zoo";
		var zooModule = System.IO.Path.Combine(zooDir, "zoo");

		var exports = _jintVirtualMachine.Execute(zooModule);
		var zooSize = exports.AsNumber();
		Assert.AreEqual(2, zooSize);
	}

	[Test()]
	public void JavascriptRequire()
	{
		var jsDir = "./data/javascript";
		var jsFilePath = System.IO.Path.Combine(jsDir, "example");

		var jsValue = _jintVirtualMachine.Execute(jsFilePath);

		Console.WriteLine("jsValue: " + jsValue);
	}

	public string GetCsProjDirectory()
	{
		string workingDirectory = Environment.CurrentDirectory;
		string projectDirectory = System.IO.Directory.GetParent(workingDirectory).Parent.Parent.FullName;
		return projectDirectory;
	}

	[Test()]
	public void JavascriptErrorThrownFromModule()
	{
		var rootFilePath = GetCsProjDirectory();
		var jsFilePath = System.IO.Path.Combine(rootFilePath, "Generated", "Javascript", "throwInModuleMain");

		var ex = Assert.Throws<Javascript.Jint.JintJsException>(() =>
		{
			var jsValue = _jintVirtualMachine.Execute(jsFilePath);
		});

		Console.WriteLine("ex: " + ex);
		StringAssert.Contains("throwInModuleMain", ex.ModuleName);
	}

}
