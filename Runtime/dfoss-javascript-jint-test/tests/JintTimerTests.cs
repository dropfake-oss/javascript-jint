using System;
using NUnit.Framework;
using System.Threading.Tasks;
using Javascript.Core;
using Javascript.Jint;

public class JintTimerTests
{
	private Javascript.Core.IJavascriptVirtualMachine _jintVirtualMachine;
	private JintRunner _jintRunner;

	private IJavascriptLogger _logger;

	private bool _running = false;
	//private Task _task;
	private int _jintTickIntervalMillis = 10;

	public JintTimerTests()
	{
		_logger = new DefaultLogger();
		//_logger = new StringLogger();
		var jintFileSystem = new Javascript.Core.InMemoryFileSystem();
		_jintRunner = new JintRunner(_logger, jintFileSystem);
		_jintVirtualMachine = _jintRunner.CreateJavascriptVirtualMachine("hello");
		_jintVirtualMachine.UnhandledException += JintVirtualMachine_UnhandledException;
		_running = true;
		//_logger.Debug("before Task.Run");
	}

	private Exception? _asyncException;
	private void JintVirtualMachine_UnhandledException(object sender, UnhandledExceptionEventArgs e)
	{
		if (e == null)
		{
			throw new System.Exception("UnhandledExceptionEventArgs is null!");
		}
		var exceptionObj = e.ExceptionObject;
		if (exceptionObj == null)
		{
			throw new System.Exception("UnhandledExceptionEventArgs.ExceptionObject is null");
		}
		Exception? exception = exceptionObj as Exception;
		if (exception == null)
		{
			throw new System.Exception("UnhandledExceptionEventArgs.ExceptionObject is not of type Exception!");
		}

		_asyncException = exception;
	}

	[OneTimeSetUp]
	public async Task BeforeAll()
	{
		Console.WriteLine("in BeforeAll");
	}

	[OneTimeTearDown]
	protected void TearDown()
	{
		_running = false;
		// TPC: not sure if there is a manual way to force abort the task
		/*try
		{
			//_task.
		}
		catch (System.Exception ex)
		{
			_logger.Debug("exception: " + ex);
		}
		*/

		var stringLogger = _logger as StringLogger;
		if (stringLogger != null)
		{
			stringLogger.Flush();
		}
	}

	[SetUp]
	protected void BeforeEach()
	{
		_running = true;
	}

	private void TickJint(int timeout)
	{
		// TPC: the following is an "unsafe way" of "ticking" JINT - because JINT is not thread safe
		/*_task = Task.Run(() =>
		{
			while (_running)
			{
				//_logger.Debug("in Task.Run");
				_jintRunner.Update(_jintTickIntervalMillis);
				Task.Delay(_jintTickIntervalMillis);
			}
		});
		*/
		System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
		stopwatch.Start();
		while (_running)
		{
			//_logger.Debug("in Task.Run");
			_jintRunner.Update(_jintTickIntervalMillis);
			if (stopwatch.Elapsed.Milliseconds >= timeout)
			{
				break;
			}
			System.Threading.Thread.Sleep(_jintTickIntervalMillis);
		}
	}

	private uint _methodCallCounter = 0;

	delegate void StringFormatDelegate(string msg, params object?[] args);

	private void StringFormat(string msg, params object?[] args)
	{
		++_methodCallCounter;
		_logger.Debug(msg);
	}

	[Test()]
	public void JavascriptSetTimeout()
	{
		_methodCallCounter = 0;
		StringFormatDelegate stringFormatDelegate = StringFormat;
		_jintVirtualMachine.SetValue("testMethod", stringFormatDelegate);

		_jintVirtualMachine.EvalInMem("SetTimeout.js", @"
			// TPC: adding scope block here - since we declare/define counter in both tests (at global level)
			{
				let counter = 0;
				var callback = function(){
					++counter;
					console.log('js callback: counter: ' + counter);
					testMethod('neat');
				}

				setTimeout(callback, 50);
			}
		");

		// TPC: previously - we were driving JINT "ticks" with a Task.Run(..) - but JINT isn't thread-safe - so now we need to manually tick JINT on main thread
		//System.Threading.Thread.Sleep(1000);
		/*for (int i = 0; (i < 200) && (_methodCallCounter < 1); ++i)
		{
			System.Threading.Thread.Sleep(20);
		}
		*/
		System.Threading.Tasks.Task.Run(() =>
		{
			while (_running)
			{
				if (_methodCallCounter >= 1)
				{
					_running = false;
				}
				Task.Delay(_jintTickIntervalMillis);
			}
		});
		TickJint(2000);

		Assert.AreEqual(1, _methodCallCounter);
	}

	[Test()]
	public void JavascriptSetInterval()
	{
		_methodCallCounter = 0;
		//Debug.Log("`JintTimerTests: engine: " + (engine != null));
		StringFormatDelegate stringFormatDelegate = StringFormat;
		_jintVirtualMachine.SetValue("testMethod", stringFormatDelegate);

		_jintVirtualMachine.EvalInMem("SetInterval.js", @"
			let counter = 0;
			//log('JintTimerTests: 4');
			let timerId = setInterval(function(){
				++counter;
				//log('in callback in setInterval: counter: ' + counter);
				if (counter  > 3){
					clearInterval(timerId);
				}
				testMethod('neat');
			}, 50);
		");

		// TPC: previously - we were driving JINT "ticks" with a Task.Run(..) - but JINT isn't thread-safe - so now we need to manually tick JINT on main thread
		//System.Threading.Thread.Sleep(2000);
		/*for (int i = 0; (i < 200) && (_methodCallCounter < 4); ++i)
		{
			System.Threading.Thread.Sleep(20);
		}
		*/
		System.Threading.Tasks.Task.Run(() =>
		{
			while (_running)
			{
				if (_methodCallCounter >= 4)
				{
					_running = false;
				}
				Task.Delay(_jintTickIntervalMillis);
			}
		});
		TickJint(2000);

		Assert.AreEqual(4, _methodCallCounter);
	}

	public class SomeClass
	{

	}

	delegate int RegisterWithMeBroDelegate(string text, SomeClass obj);
	class RegisterWithMeBro
	{
		static RegisterWithMeBroDelegate _func;

		static SomeClass _obj;

		public static void Register(RegisterWithMeBroDelegate func)
		{
			//Console.WriteLine("Register: " + func);
			_func = func;
		}

		public static void Run()
		{
			if (_func != null)
			{
				_obj = new SomeClass();
				var result = _func("asdfklhj", _obj);
				//Console.WriteLine("result: " + result);
			}
		}
	}

	// TPC: the following was an early attempt to replicate the "<parameter name> is not defined" error that was popping up non-deterministically
	// when JINT was integrated in Unity.  As it turned out - it is due to the fact that JINT is not thread safe.
	// So all callbacks, method calls, etc must occur on the same thread as the thread that created JINT
	[Test()]
	public void JavascriptCallbackAfterDelay()
	{
		var cSharpMethodCalled = false;
		var logString = "";
		_jintVirtualMachine.SetValue("numInStringOut2",
		  new Func<int, string>(number =>
		  {
			  cSharpMethodCalled = true;
			  //_logger.Debug("numInStringOut2: number: " + number);
			  //Console.WriteLine("numInStringOut2: number: " + number);
			  logString += "numInStringOut2: number: " + number;
			  //Assert.AreEqual(magicNum, number);
			  return "C# can see that you passed: " + number;
		  })
		);

		_jintVirtualMachine.SetValue("RegisterWithMeBro", typeof(RegisterWithMeBro));

		_jintVirtualMachine.EvalInMem("CallbackAfterTimeout.js", @"
			// TPC: adding scope block here - since we declare/define counter in both tests (at global level)
			{
				let counter = 0;
				var callback = function(){
					++counter;
					//console.log('js callback: counter: ' + counter);
					var result = numInStringOut2(42);
					//console.log('js callback: result: ' + result);
				}

				RegisterWithMeBro.Register(function(a, b){
					//console.log('RegisterWithMeBro.Register: a:', a,'b:',b);
					return 52;
				});

				//console.log('before setTimeout');
				setTimeout(callback, 5);
				//console.log('after setTimeout');
			}
		");

		RegisterWithMeBro.Run();

		//Console.WriteLine("JavascriptCallbackAfterDelay: after JVM.Eval");
		// TPC: previously - we were driving JINT "ticks" with a Task.Run(..) - but JINT isn't thread-safe - so now we need to manually tick JINT on main thread
		/*for (int i = 0; (i < 200) && (!cSharpMethodCalled); ++i)
		{
			System.GC.Collect();
			System.Threading.Thread.Sleep(10);
			//RegisterWithMeBro.Run();
		}
		*/
		System.Threading.Tasks.Task.Run(() =>
		{
			while (_running)
			{
				System.GC.Collect();
				if (cSharpMethodCalled)
				{
					_running = false;
				}
				Task.Delay(_jintTickIntervalMillis);
			}
		});
		TickJint(2000);

		RegisterWithMeBro.Run();

		//Console.WriteLine("JavascriptCallbackAfterDelay: _asyncException: " + _asyncException);
		//Console.WriteLine("JavascriptCallbackAfterDelay: after Thread.Sleep: logString: " + logString);
		Assert.AreEqual(true, cSharpMethodCalled);
	}

	[Test()]
	public void JavascriptThrowInSetTimeout()
	{
		StringFormatDelegate stringFormatDelegate = StringFormat;
		_jintVirtualMachine.SetValue("testMethod", stringFormatDelegate);

		_jintVirtualMachine.EvalInMem("ThrowErrorInSetTimeout.js", @"
			// TPC: adding scope block here - since we declare/define counter in both tests (at global level)
			{
				let counter = 0;
				var callback = function(){
					++counter;
					//console.log('js callback: counter: ' + counter);
					throw new Error('this is an awesome error');
					testMethod('neat');
				}

				setTimeout(callback, 10);
			}
		");

		//Console.WriteLine("JavascriptThrowInSetTimeout: after JVM.Eval: _asyncException: " + _asyncException);
		// TPC: previously - we were driving JINT "ticks" with a Task.Run(..) - but JINT isn't thread-safe - so now we need to manually tick JINT on main thread
		//System.Threading.Thread.Sleep(1000);
		/*
		for (int i = 0; (i < 200) && (_asyncException == null); ++i)
		{
			System.Threading.Thread.Sleep(10);
		}
		*/
		System.Threading.Tasks.Task.Run(() =>
		{
			while (_running)
			{
				System.GC.Collect();
				if (_asyncException != null)
				{
					_running = false;
				}
				Task.Delay(_jintTickIntervalMillis);
			}
		});

		Assert.Throws<Jint.Runtime.JavaScriptException>(() =>
		{
			TickJint(2000);
		});


		//Console.WriteLine("JavascriptThrowInSetTimeout: after Thread.Sleep: _asyncException: " + _asyncException);
		Assert.IsInstanceOf<Jint.Runtime.JavaScriptException>(_asyncException);
	}

	delegate int RandoDelegate(string text, SomeClass obj);

	class CallJavascriptCallback
	{
		static RandoDelegate _func;

		static SomeClass _obj;

		public static void Register(RandoDelegate func)
		{
			_func = func;
		}

		public static void Run()
		{
			if (_func != null)
			{
				_obj = new SomeClass();
				var result = _func("asdfklhj", _obj);
			}
		}
	}


	// TPC: the following test won't pass depending on the version of .NET we target for the generation of the Jint.dll - i.e. we no longer get an exception thrown when we use JINT across multiple threads
	//[Test()]
	public void JintShouldThrowWhenUsedAcrossThreads()
	{
		var cSharpMethodCalled = false;

		_jintVirtualMachine.SetValue("numInStringOut3",
			new Func<int, string>(number =>
			{
				cSharpMethodCalled = true;
				return "C# can see that you passed: " + number;
			})
		);

		_jintVirtualMachine.SetValue("CallJavascriptCallback", typeof(CallJavascriptCallback));

		var setTimeoutCompleted = false;
		_jintVirtualMachine.SetValue("setTimeout", new Action<Action, int>(async (action, msec) =>
		{
			var task = System.Threading.Tasks.Task.Run(() =>
			{
				System.Threading.Tasks.Task.Delay(msec);
				action();
			});

			var exception = Assert.ThrowsAsync<System.Exception>(async () =>
			{
				await task;
			});

			//Assert.Contains("JINT is not thread-safe!", exception.Message);
			setTimeoutCompleted = true;
		}));

		_jintVirtualMachine.EvalInMem("crossThreadTest.js", @"
			    {
				    let counter = 0;
				    var callback = function(){
					    ++counter;
					    var result = numInStringOut3(42);
				    }

				    CallJavascriptCallback.Register(function(a, b){
                        if (a != 'asdfklhj'){
                            throw new Error('CallJavascriptCallback: a does not have expected value - it equals: ' + a);
                        }
					    return 52;
				    });

				    setTimeout(callback, 5);
			    }
		    ");

		try
		{
			CallJavascriptCallback.Run();
		}
		catch
		{
			// Currently this Exception is JavaScriptException "a is not defined" - but the authors of JINT may change what exception is thrown in this situation - so not testing that
		}
		for (int i = 0; (i < 100) && (!setTimeoutCompleted); ++i) // wait until setTimeout callback completes - in this case it will throw
		{
			System.Threading.Thread.Sleep(10);
		}
	}
}
