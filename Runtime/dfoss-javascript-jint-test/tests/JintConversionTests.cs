using System;
using NUnit.Framework;
using System.Threading.Tasks;
using Javascript.Core;
using Javascript.Jint;

public class JintConversionTests
{
	private Javascript.Core.IJavascriptVirtualMachine VM;

	private IJavascriptLogger Logger;

	public JintConversionTests()
	{
		this.Logger = new DefaultLogger();
		var jintFileSystem = new Javasscript.Jint.Tests.JintTestFileSystem();
		this.VM = new Javascript.Jint.JintVirtualMachine(this.Logger, jintFileSystem);
	}

	private uint UniqueCounter = 0;
	private string GetUniqueModule()
	{
		return String.Format("conversionModule_{0}", (this.UniqueCounter++));
	}

	[Test()]
	public void BoolTests()
	{
		var notBool = GetUniqueModule();
		this.VM.RegisterFunction(notBool, @"return 3;");
		var v = this.VM.InvokeFunction(notBool);
		Assert.AreEqual(false, this.VM.IsBool(v));
		try
		{
			this.VM.AsBool(v);
		}
		catch (InvalidCastException ex)
		{
			Assert.AreEqual(true, ex.Message.Contains("obj is not a JsBoolean"));
		}
		Assert.AreEqual(false, this.VM.TryAsBool(v, out bool asBoolNot));


		var isTrue = GetUniqueModule();
		this.VM.RegisterFunction(isTrue, @"return true;");
		v = this.VM.InvokeFunction(isTrue);
		Assert.AreEqual(true, this.VM.IsBool(v));
		Assert.AreEqual(true, this.VM.AsBool(v));
		Assert.AreEqual(true, this.VM.TryAsBool(v, out bool asBoolTrue));
		Assert.AreEqual(true, asBoolTrue);

		var isFalse = GetUniqueModule();
		this.VM.RegisterFunction(isFalse, @"return false;");
		v = this.VM.InvokeFunction(isFalse);
		Assert.AreEqual(true, this.VM.IsBool(v));
		Assert.AreEqual(false, this.VM.AsBool(v));
		Assert.AreEqual(true, this.VM.TryAsBool(v, out bool asBoolFalse));
		Assert.AreEqual(false, asBoolFalse);
	}

	[Test()]
	public void NumericTests()
	{
		var notNumber = GetUniqueModule();
		this.VM.RegisterFunction(notNumber, @"return true;");
		var v = this.VM.InvokeFunction(notNumber);
		Assert.AreEqual(false, this.VM.IsNumber(v));
		try
		{
			this.VM.AsNumber(v);
		}
		catch (InvalidCastException ex)
		{
			Assert.AreEqual(true, ex.Message.Contains("obj is not a JsNumber"));
		}
		Assert.AreEqual(false, this.VM.TryAsNumber(v, out double asNumberNot));


		var isInt = GetUniqueModule();
		this.VM.RegisterFunction(isInt, @"return 3;");
		v = this.VM.InvokeFunction(isInt);
		Assert.AreEqual(true, this.VM.IsNumber(v));
		Assert.AreEqual(3, this.VM.AsNumber(v));
		Assert.AreEqual(true, this.VM.TryAsNumber(v, out double asNumberInt));
		Assert.AreEqual(3, asNumberInt);

		var isDouble = GetUniqueModule();
		this.VM.RegisterFunction(isDouble, @"return 4.5;");
		v = this.VM.InvokeFunction(isDouble);
		Assert.AreEqual(true, this.VM.IsNumber(v));
		Assert.AreEqual(4.5, this.VM.AsNumber(v));
		Assert.AreEqual(true, this.VM.TryAsNumber(v, out double asNumberDouble));
		Assert.AreEqual(4.5, asNumberDouble);
	}
}
