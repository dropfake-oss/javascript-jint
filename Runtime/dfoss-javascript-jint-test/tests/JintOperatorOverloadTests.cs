using NUnit.Framework;
using System;

using Javascript.Core;

using FixedPointTesting;


[TestFixture()]
public class JintOperatorOverloadTests
{
	private IJavascriptVirtualMachine VM;
	private IJavascriptLogger Logger;

	public JintOperatorOverloadTests()
	{
		var assemblyNamespaces = new AssemblyNamespaces[] {
			new AssemblyNamespaces(typeof(JintAsTypeTests).Assembly, new string[] { "FixedPointTesting" }),
		};

		this.Logger = new DefaultLogger();
		var jintFileSystem = new Javasscript.Jint.Tests.JintTestFileSystem();
		this.VM = new Javascript.Jint.JintVirtualMachine(this.Logger, jintFileSystem, assemblyNamespaces);
	}

	public struct A
	{
		public int Value { get; set; } = 0;
		public A(int value) { Value = value; }
		public static A operator +(A lhs, A rhs) { return new A(lhs.Value + rhs.Value); }
		public static A operator -(A lhs, A rhs) { return new A(lhs.Value - rhs.Value); }
		public static A operator *(A lhs, A rhs) { return new A(lhs.Value * rhs.Value); }
		public static A operator /(A lhs, A rhs) {
			return new A(lhs.Value / rhs.Value);
		}
		public override string ToString() {
			return Value.ToString();
		}
	}

	[Test()]
	public void Add()
	{
		var lines = new string[]
		{
			"const third = first + second;",
		 	"return third;"
		};

		var first = new A(7);
		var second = new A(6);

		var output = this.VM.RegisterAndInvokeFunction("add", SourceUtils.LinesToSourceCode(lines), "first, second", first, second);
		var isStruct = this.VM.IsStruct<A>(output);
		Assert.AreEqual(true, isStruct);
		var third = this.VM.AsStruct<A>(output);
		Assert.AreEqual(13, third.Value);
	}

	[Test()]
	public void Sub()
	{
		var lines = new string[]
		{
			"const third = first - second;",
		 	"return third;"
		};

		var first = new A(21);
		var second = new A(6);

		var output = this.VM.RegisterAndInvokeFunction("sub", SourceUtils.LinesToSourceCode(lines), "first, second", first, second);
		var isStruct = this.VM.IsStruct<A>(output);
		Assert.AreEqual(true, isStruct);
		var third = this.VM.AsStruct<A>(output);
		Assert.AreEqual(15, third.Value);
	}

	[Test()]
	public void Mult()
	{
		var lines = new string[]
		{
			"const third = first * second;",
		 	"return third;"
		};

		var first = new A(7);
		var second = new A(6);

		var output = this.VM.RegisterAndInvokeFunction("mult", SourceUtils.LinesToSourceCode(lines), "first, second", first, second);
		var isStruct = this.VM.IsStruct<A>(output);
		Assert.AreEqual(true, isStruct);
		var third = this.VM.AsStruct<A>(output);
		Assert.AreEqual(42, third.Value);
	}

	[Test()]
	public void Div()
	{
		var lines = new string[]
		{
			"const third = first / second;",
		 	"return third;"
		};

		var first = new A(12);
		var second = new A(6);

		var output = this.VM.RegisterAndInvokeFunction("div", SourceUtils.LinesToSourceCode(lines), "first, second", first, second);
		var isStruct = this.VM.IsStruct<A>(output);
		Assert.AreEqual(true, isStruct);
		var third = this.VM.AsStruct<A>(output);
		Assert.AreEqual(2, third.Value);
	}

	public class HitResult
	{
		public int AttackType;
		public FP Damage;
	}

	[Test()]
	public void Addition()
	{
		var lines = new string[]
		{
		 	"let adjustment = FixedPointTesting.FP._1;",
		 	"switch(hit.AttackType) {",
		 	"	case 1:",
		 	"		adjustment = delta + FixedPointTesting.FP._1;",
		 	"		break;",
		 	"	case 2:",
		 	"		adjustment = delta + FixedPointTesting.FP._3;",
		 	"		break;",
		 	"	default:",
		 	"		break;",
		 	"}",
		 	"return adjustment;",
		};

		var hit = new HitResult();
		hit.AttackType = 2;
		hit.Damage = FP._4;

		var delta = FP._100;

		var output = this.VM.RegisterAndInvokeFunction("addition", SourceUtils.LinesToSourceCode(lines), "delta, hit", delta, hit);
		var isStruct = this.VM.IsStruct<FP>(output);
		Assert.AreEqual(true, isStruct);
		var adjustment = this.VM.AsStruct<FP>(output);
		Assert.AreEqual(FixedPointTesting.FP.FromFloat_UNSAFE(103.0f), adjustment);

		FP fromTry;
		isStruct = this.VM.TryAsStruct<FP>(output, out fromTry);
		Assert.AreEqual(true, isStruct);
		Assert.AreEqual(FixedPointTesting.FP.FromFloat_UNSAFE(103.0f), fromTry);
	}

}
