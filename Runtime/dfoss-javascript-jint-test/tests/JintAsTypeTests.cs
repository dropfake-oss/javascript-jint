using NUnit.Framework;

using Javascript.Core;

using FixedPointTesting;


[TestFixture()]
public class JintAsTypeTests
{
	private IJavascriptVirtualMachine VM;
	private IJavascriptLogger Logger;

	public JintAsTypeTests()
	{
		var assemblyNamespaces = new AssemblyNamespaces[] {
			new AssemblyNamespaces(typeof(JintAsTypeTests).Assembly, new string[] { "FixedPointTesting" }),
		};

		this.Logger = new DefaultLogger();
		var jintFileSystem = new Javasscript.Jint.Tests.JintTestFileSystem();
		this.VM = new Javascript.Jint.JintVirtualMachine(this.Logger, jintFileSystem, assemblyNamespaces);
	}

	public class HitResult
	{
		public int AttackType;
		public FP Damage;
	}

	[Test()]
	public void AsStruct()
	{
		var lines = new string[]
		{
				"let adjustment = FixedPointTesting.FP._2;",
				"return adjustment;"
		};

		var hit = new HitResult();
		hit.AttackType = 2;
		hit.Damage = FP._4;

		var delta = FP._100;

		var output = this.VM.RegisterAndInvokeFunction("as_struct", SourceUtils.LinesToSourceCode(lines), "delta, hit", delta, hit);
		var isStruct = this.VM.IsStruct<FP>(output);
		Assert.AreEqual(true, isStruct);
		var adjustment = this.VM.AsStruct<FP>(output);
		Assert.AreEqual(adjustment, FixedPointTesting.FP._2);

		FP fromTry;
		isStruct = this.VM.TryAsStruct<FP>(output, out fromTry);
		Assert.AreEqual(true, isStruct);
		Assert.AreEqual(fromTry, FixedPointTesting.FP._2);
	}

	[Test()]
	public void AsClass()
	{
		var lines = new string[]
		{
				"hit.AttackType = hit.AttackType + 1",
				"return hit;"
		};

		var hit = new HitResult();
		hit.AttackType = 2;
		hit.Damage = FP._4;

		var delta = FP._100;

		var output = this.VM.RegisterAndInvokeFunction("as_class", SourceUtils.LinesToSourceCode(lines), "delta, hit", delta, hit);
		var isClass = this.VM.IsClass<HitResult>(output);
		Assert.AreEqual(true, isClass);
		var updatedHit = this.VM.AsClass<HitResult>(output);
		Assert.AreEqual(updatedHit?.AttackType, 3);

		isClass = this.VM.TryAsClass<HitResult>(output, out HitResult? fromTry);
		Assert.AreEqual(true, isClass);
		Assert.AreEqual(fromTry?.AttackType, 3);
	}
}
