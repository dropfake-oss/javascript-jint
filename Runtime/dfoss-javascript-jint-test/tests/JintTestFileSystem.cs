using System;
using System.IO;

namespace Javasscript.Jint.Tests
{
	public class JintTestFileSystem : Javascript.Core.IFileSystem
	{
		DirectoryInfo _rootDirInfo;
		DirectoryInfo _javascriptDirInfo;

		private const string jsFileExt = ".js";

		public JintTestFileSystem()
		{
			SetJavascriptDataDirInfo();

			if (_rootDirInfo == null)
			{
				throw new System.NullReferenceException("JintTestFileSystem: _rootDirInfo must not be null when constructor completes!");
			}
			if (_javascriptDirInfo == null)
			{
				throw new System.NullReferenceException("JintTestFileSystem: _javascriptDirInfo must not be null when constructor completes!");
			}
		}

		private void SetJavascriptDataDirInfo()
		{
			var currentDir = System.IO.Directory.GetCurrentDirectory();
			// /Users/tcassidy/gitlab/javascript-jint/Runtime/dfoss-javascript-jint-test/bin/Debug/net5.0
			Console.WriteLine("JavascriptRequire: currentDir: " + currentDir);

			DirectoryInfo dirInfo = new System.IO.DirectoryInfo(currentDir);
			_rootDirInfo = dirInfo;

			var dataDirInfos = dirInfo.GetDirectories("data");

			var dataDirInfo = dataDirInfos[0];
			var javascriptDirInfos = dataDirInfo.GetDirectories("javascript");

			var javascriptDirInfo = javascriptDirInfos[0];
			_javascriptDirInfo = javascriptDirInfo;
		}

		public bool Exists(string path)
		{
			var fullPath = Path.Combine(_javascriptDirInfo.FullName, path);
			var fileExists = File.Exists(fullPath);
			Console.WriteLine("JintTestFileSystem: Exists: path: " + path + " fullPath: " + fullPath + " fileExists: " + fileExists);
			//return File.Exists(fullPath);

			fullPath = Path.Combine(_rootDirInfo.FullName, path);
			fileExists = File.Exists(fullPath);
			Console.WriteLine("JintTestFileSystem: Exists(2): path: " + path + " fullPath: " + fullPath + " fileExists: " + fileExists);
			return fileExists;
		}

		public byte[] ReadAllBytes(string path)
		{
			Console.WriteLine("JintTestFileSystem: ReadAllBytes: path: " + path);
			if (Path.IsPathRooted(path))
			{
				return File.ReadAllBytes(path);
			}
			//var fullPath = Path.Combine(_javascriptDirInfo.FullName, path);
			var fullPath = Path.Combine(_rootDirInfo.FullName, path);
			if (!fullPath.EndsWith(jsFileExt))
			{
				fullPath = fullPath + jsFileExt;
			}
			Console.WriteLine("JintTestFileSystem: ReadAllBytes: path: " + path + " fullPath: " + fullPath);
			return File.ReadAllBytes(fullPath);
		}

		public void WriteFile(string path, string contents)
		{
			throw new System.NotImplementedException("do we need to support WriteFile in this context (path: " + path + "!");
		}

		public void WriteFile(string path, byte[] bytes)
		{
			throw new System.NotImplementedException("do we need to support WriteFile in this context (path: " + path + "!");
		}

	}
}
