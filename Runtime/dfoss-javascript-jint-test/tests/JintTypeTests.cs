using Jint.Runtime;
using System;
using NUnit.Framework;
using Javascript.Jint;
using Javascript.Core;

namespace Foo.Bar
{
	class Dingaling
	{
		public static uint Counter = 0;

		public void Write()
		{
			++Counter;
		}
	}
}

[TestFixture()]
public class JintTypeTests
{
	private JintVirtualMachine _jintVm;
	private IJavascriptLogger _logger;

	delegate void DynamicParamsDelegate(params object[] args);

	public JintTypeTests()
	{
		_logger = new DefaultLogger();
		var jintFileSystem = new Javascript.Core.InMemoryFileSystem();
		_jintVm = new JintVirtualMachine(_logger, jintFileSystem);

		GameApi.Logger = _logger;
	}

	private bool _methodCalled = false;
	private void DynamicParams(params object[] args)
	{
		_methodCalled = true;
		Assert.AreEqual(2, args.Length);
		foreach (var obj in args)
		{
			_logger.Debug("DynamicParams: obj: " + obj);
		}
	}

	private uint _fileCounter = 0;
	private string GetUniqueFileBase()
	{
		return String.Format("jsFile{0}", (_fileCounter++));
	}
	private string GetUniqueFileName()
	{
		return GetUniqueFileBase() + ".js";
	}

	[Test()]
	public void JavascriptCallsCSharpMethod()
	{
		//engine.SetValue("log", new Action<object>(msg => _logger.Debug(msg)));

		var magicNum = 42;
		bool cSharpMethodCalled = false;
		_jintVm.SetValue("numInStringOut",
		  new Func<int, string>(number =>
		  {
			  cSharpMethodCalled = true;
			  _logger.Debug("numInStringOut: number: " + number);
			  Assert.AreEqual(magicNum, number);
			  return "C# can see that you passed: " + number;
		  })
		);

		string script = $@"
			var randoNum = 108;
			console.log('Hello from Javascript! randoNum = ' + randoNum);
			let randoNum2 = {magicNum};
			let doesLetWork = numInStringOut(randoNum2);
			console.log('doesLetWork:' + doesLetWork);
		";

		// TPC: adding scope block here - since we declare/define counter in both tests (at global level)
		_jintVm.EvalInMem(GetUniqueFileName(), "{" + Environment.NewLine + script + Environment.NewLine + "}");
		Assert.AreEqual(true, cSharpMethodCalled);
	}

	[Test()]
	public void JavascriptCallsCSharpMethodWithVariableNumArgs()
	{
		_methodCalled = false;
		// TPC: the following is how we deal with functions with an arbitrary number of arguments
		DynamicParamsDelegate dynamicParamsDelegate = DynamicParams;
		_jintVm.SetValue("testParams", dynamicParamsDelegate);

		_jintVm.EvalInMem(GetUniqueFileName(), @"
			{ // TPC: adding scope block here - since we declare/define counter in both tests (at global level)
				var randoNum = 108;
				let randoNum2 = 42;

				testParams(randoNum, randoNum2);
			}
		");

		Assert.AreEqual(true, _methodCalled);
	}

	private class GameApi
	{
		public static IJavascriptLogger Logger = null!;
		public static uint ApiMethod1Count = 0;
		public void ApiMethod1()
		{
			++ApiMethod1Count;
			Logger.Debug("Called api method 1");
		}

		public static uint ApiMethod2Count = 0;
		public int ApiMethod2()
		{
			++ApiMethod2Count;
			Logger.Debug("Called api method 2");
			return 2;
		}

		public static int Method3Value = 0;
		public void ApiMethod3(int v)
		{
			Method3Value = v;
		}

		public static void Reset()
		{
			ApiMethod1Count = 0;
			ApiMethod2Count = 0;
			Method3Value = 0;
		}
	}

	[Test()]
	public void JavascriptUsesCSharpClassInstance()
	{
		GameApi.Reset();
		// the following TypeReference is in the Jint.Runtime.Interop namespace
		_jintVm.SetValue("GameApi", typeof(GameApi));

		_jintVm.EvalInMem(GetUniqueFileName(), @"
			{ // TPC: adding scope block here - since we declare/define counter in both tests (at global level)
				var gameApi = new GameApi();
				gameApi.ApiMethod1();
				var result = gameApi.ApiMethod2();
				console.log('output from ApiMethod2: ' + result);
			}
		");

		Assert.AreEqual(1, GameApi.ApiMethod1Count);
		Assert.AreEqual(1, GameApi.ApiMethod2Count);
	}

	[Test()]
	public void JavascriptHandleUnknownVariable()
	{
		string errMsg = "";
		try
		{
			_jintVm.EvalInMem(GetUniqueFileName(), @"
				function throwUnknownVariableError(){
					let numVar = doesNotExistVar;
					console.log('thisThrowsError');
				}
				throwUnknownVariableError();
			");
		}
		// TPC: TODO: currently the JavaScriptException.Location setter property in Jint is private - so our instance of JavaScriptException.Location is null/unset - so we can't catch JavaScriptException
		/*
		 catch (JavaScriptException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.LineNumber}, Column {ex.Column})";
			errMsg = error;
			_logger.Debug(error);
		}
		*/
		catch (JintJsException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.LineNumber}, Column {ex.Column})";
			errMsg = error;
			_logger.Debug(error);
			_logger.Debug("Exception.Message: " + ex.Message);
		}
		catch (Exception ex)
		{
			throw new ApplicationException($"Error: {ex.Message}");
		}

		Assert.AreEqual("Jint runtime error ReferenceError: doesNotExistVar is not defined (Line 3, Column 18)", errMsg);
	}

	[Test()]
	public void JavascriptHandleThrownError()
	{
		string errMsg = "";
		try
		{
			_jintVm.EvalInMem(GetUniqueFileName(), @"
				function thisThrowsError(){
					throw new Error('just testing error handling');
				}

				thisThrowsError();
			");
		}
		// TPC: TODO: currently the JavaScriptException.Location setter property in Jint is private - so our instance of JavaScriptException.Location is null/unset - so we can't catch JavaScriptException
		/*
		 catch (JavaScriptException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.LineNumber}, Column {ex.Column})";
			errMsg = error;
			_logger.Debug(error);
		}
		*/
		catch (JintJsException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.LineNumber}, Column {ex.Column})";
			errMsg = error;
			_logger.Debug(error);
			_logger.Debug("Exception.Message: " + ex.Message);
		}
		catch (Exception ex)
		{
			throw new ApplicationException($"Error: {ex.Message}");
		}

		Assert.AreEqual("Jint runtime error Error: just testing error handling (Line 3, Column 11)", errMsg);
	}

	[Test()]
	public void CallPreviouslyRegisteredCode()
	{
		GameApi.Reset();
		_jintVm.SetValue("GameApi", typeof(GameApi));

		var name = GetUniqueFileBase() + "_registeredFunc";
		var source = @"
	var gameApi = new GameApi();
	gameApi.ApiMethod1();
";
		_jintVm.RegisterFunction(name, source);

		_jintVm.EvalInMem(GetUniqueFileName(), name + "()");
		Assert.AreEqual(1, GameApi.ApiMethod1Count);

		_jintVm.InvokeFunction(name);
		Assert.AreEqual(2, GameApi.ApiMethod1Count);
	}

	[Test()]
	public void CallPreviouslyRegisteredCodeAndDoNotStomp()
	{
		GameApi.Reset();
		_jintVm.SetValue("GameApi", typeof(GameApi));

		var name = GetUniqueFileBase() + "_registeredFunc";
		var source = @"
	console.log('funcWithArgs: arg1:',arg1,'arg2:',arg2);
	var gameApi = new GameApi();
	gameApi.ApiMethod1();
";
		_jintVm.RegisterFunction(name, source, "arg1, arg2");

		// The following js code will _not_ be registered - since we've already registere
		_jintVm.RegisterFunction(name, @"throw new Error('this error will never be thrown - because this code will never be registered');");

		_jintVm.Invoke(name, 42, "foo");
		Assert.AreEqual(1, GameApi.ApiMethod1Count);
	}

	[Test()]
	public void InvalidFunctionNameTest()
	{
		var exception = false;

		var name = "foo/" + GetUniqueFileBase() + "_registeredFunc";
		try
		{
			_jintVm.RegisterAndInvokeFunction(name, "", "arg1, arg2", 42, "foo");
		}
		catch (JavascriptInvalidFunctionNameException ex)
		{
			Assert.True(ex.Message.Contains("contains invalid characters"));
			exception = true;
		}

		if (exception == false)
		{
			Assert.Fail("Should have thrown JavascriptInvalidFunctionNameException");
		}
	}

	[Test()]
	public void FunctionParsingErrorTest()
	{
		var exception = false;

		var name = GetUniqueFileBase() + "_registeredFunc";
		try
		{
			var source = @"
	let let = 'a';
	return let;
";
			_jintVm.RegisterAndInvokeFunction(name, source, "arg1, arg2", 42, "foo");
		}
		catch (JavascriptFunctionParsingException ex)
		{
			Assert.True(ex.Message.Contains("Line 3: Unexpected token let"));
			exception = true;
		}

		if (exception == false)
		{
			Assert.Fail("Should have thrown JavascriptFunctionParsingException");
		}
	}

	[Test()]
	public void RegisterAndInvokeFunctionTest()
	{
		GameApi.Reset();
		_jintVm.SetValue("GameApi", typeof(GameApi));

		var name = GetUniqueFileBase() + "_registeredFunc";
		var source = @"
	var gameApi = new GameApi();
	gameApi.ApiMethod3(arg1);
";
		_jintVm.RegisterAndInvokeFunction(name, source, "arg1, arg2", 42, "foo");
		Assert.AreEqual(42, GameApi.Method3Value);
	}

	[Test()]
	public void RegisterAndInvokeWithNullsFunctionTest()
	{
		GameApi.Reset();
		_jintVm.SetValue("GameApi", typeof(GameApi));

		var name = GetUniqueFileBase() + "_registeredFunc";
		var source = @"
	var gameApi = new GameApi();
	if( isNull == null ) {
		gameApi.ApiMethod3(arg1);
	}
	if( isNull != null ) {
		gameApi.ApiMethod3(arg2);
	}
";
		_jintVm.RegisterAndInvokeFunction(name, source, "arg1, arg2, isNull", 57, 63, null);
		Assert.AreEqual(57, GameApi.Method3Value);
	}

	[Test()]
	public void JavascriptThrowErrorInSetValue()
	{
		string errMsg = "";
		try
		{
			_jintVm.EvalInMem(GetUniqueFileName(), @"
				function thisThrowsError(){
					throw new Error('just testing error handling');
				}
				thisThrowsError();
			");
		}
		// TPC: TODO: currently the JavaScriptException.Location setter property in Jint is private - so our instance of JavaScriptException.Location is null/unset - so we can't catch JavaScriptException
		/*
		 catch (JavaScriptException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.LineNumber}, Column {ex.Column})";
			errMsg = error;
			_logger.Debug(error);
		}
		*/
		catch (JintJsException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.LineNumber}, Column {ex.Column})";
			errMsg = error;
			_logger.Debug(error);
			_logger.Debug("Exception.Message: " + ex.Message);
		}
		catch (Exception ex)
		{
			throw new ApplicationException($"Error: {ex.Message}");
		}

		Assert.AreEqual("Jint runtime error Error: just testing error handling (Line 3, Column 11)", errMsg);
	}

	[Test()]
	public void JavascriptHandleUnknownFunction()
	{
		string errMsg = "";
		try
		{
			_jintVm.EvalInMem(GetUniqueFileName(), @"
				let obj = {};
				obj.foo();
			");
		}
		// TPC: TODO: currently the JavaScriptException.Location setter property in Jint is private - so our instance of JavaScriptException.Location is null/unset - so we can't catch JavaScriptException
		/*
		 catch (JavaScriptException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.LineNumber}, Column {ex.Column})";
			errMsg = error;
			_logger.Debug(error);
		}
		*/
		catch (JintJsException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.LineNumber}, Column {ex.Column})";
			errMsg = error;
			_logger.Debug(error);
			_logger.Debug("Exception.Message: " + ex.Message);
		}
		catch (Exception ex)
		{
			throw new ApplicationException($"Error: {ex.Message}");
		}

		Assert.AreEqual("Jint runtime error TypeError: Property 'foo' of object is not a function (Line 3, Column 4)", errMsg);
		//Console.WriteLine("JavascriptHandleUnknownFunction: errMsg: " + errMsg);
	}

	[Test()]
	public void JavascriptHandleUnknownMethodInBoundClass()
	{
		try
		{
			var jintFileSystem = new Javasscript.Jint.Tests.JintTestFileSystem();
			var assembly = typeof(Foo.Bar.Dingaling).Assembly;
			var assemblyNamespaces = new AssemblyNamespaces(assembly, new string[] { "Foo" });
			var assemblyNamespacesArray = new AssemblyNamespaces[] { assemblyNamespaces };
			var jintVm = new Javascript.Jint.JintVirtualMachine(_logger, jintFileSystem, assemblyNamespacesArray);
			// test passing in an instance
			var dingaling = new Foo.Bar.Dingaling();
			jintVm.SetValue("dingaling", dingaling);
			var jsFileName = GetUniqueFileName();
			jintVm.EvalInMem(jsFileName, @"
				const Foo = {};
				Foo.Bar = importNamespace('Foo.Bar');
				const test = new Foo.Bar.Dingaling();
				test.Write(); // this will increment the Dingaling.Counter
				dingaling.Write(); // this will increment the Dingaling.Counter
			");

			var isRegistered = jintVm.IsModuleRegistered(jsFileName);
			Assert.AreEqual(true, isRegistered);

			//Console.WriteLine("Foo.Bar.Dingaling.Foo.Bar.Dingaling: " + Foo.Bar.Dingaling.Counter);
			Assert.AreEqual(2, Foo.Bar.Dingaling.Counter);

			Assert.Throws<JintJsException>(() =>
			{
				jintVm.EvalInMem("someFile9.js", @"
					const dingaling = new Foo.Bar.Dingaling();
					test.DoesNotExist();
					console.log('after calling method that does not exist');
				");
			});

			//Console.WriteLine("after calling method that does not exist on bound class");
		}
		catch (JavaScriptException ex)
		{
			var error = $"Jint runtime error {ex.Error} (Line {ex.Location.Start.Line}, Column {ex.Location.Start.Column})";
			string errMsg = error;
			_logger.Debug(error);
			throw;
		}
		catch (Exception ex)
		{
			throw new ApplicationException($"Error: {ex.Message}");
		}

		//Assert.AreEqual("Jint runtime error ReferenceError: doesNotExistVar is not defined (Line 3, Column 5)", errMsg);
		//Console.WriteLine("JavascriptHandleUnknownFunction: errMsg: " + errMsg);
	}
}
