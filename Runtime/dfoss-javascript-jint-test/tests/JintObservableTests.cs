using Jint;
using Jint.Runtime.Interop;
using Jint.Runtime;
using System;
using NUnit.Framework;
using System.Threading.Tasks;
using Javascript.Core;
using Javascript.Jint;

public class JintObservableTests
{
	private Javascript.Core.IJavascriptVirtualMachine _jintVirtualMachine;

	private IJavascriptLogger _logger;

	private bool _running = false;
	private Task _task;

	public JintObservableTests()
	{
		_logger = new DefaultLogger();
		//_logger = new StringLogger();
		var jintFileSystem = new Javascript.Core.InMemoryFileSystem();
		var jintRunner = new JintRunner(_logger, jintFileSystem);
		_jintVirtualMachine = jintRunner.GetOrCreateJavascriptVirtualMachine("anotherBar");
		_jintVirtualMachine.UnhandledException += JintVirtualMachine_UnhandledException;
		_running = true;
		var numMillis = 10;

		_logger.Debug("before Task.Run");
		_task = Task.Run(() =>
		{
			while (_running)
			{
				_logger.Debug("in Task.Run");
				jintRunner.Update(numMillis);
				Task.Delay(numMillis);
			}
		});
	}

	private Exception? _asyncException;
	private void JintVirtualMachine_UnhandledException(object sender, UnhandledExceptionEventArgs e)
	{
		if (e == null)
		{
			throw new System.Exception("UnhandledExceptionEventArgs is null!");
		}
		var exceptionObj = e.ExceptionObject;
		if (exceptionObj == null)
		{
			throw new System.Exception("UnhandledExceptionEventArgs.ExceptionObject is null");
		}
		Exception? exception = exceptionObj as Exception;
		if (exception == null)
		{
			throw new System.Exception("UnhandledExceptionEventArgs.ExceptionObject is not of type Exception!");
		}

		_asyncException = exception;
	}

	[OneTimeSetUp]
	public async Task BeforeAll()
	{
		Console.WriteLine("in BeforeAll");
	}

	[OneTimeTearDown]
	protected void TearDown()
	{
		_running = false;
		// TPC: not sure if there is a manual way to force abort the task
		/*try
		{
			//_task.
		}
		catch (System.Exception ex)
		{
			_logger.Debug("exception: " + ex);
		}
		*/

		var stringLogger = _logger as StringLogger;
		if (stringLogger != null)
		{
			stringLogger.Flush();
		}
	}

	[Test()]
	public void JavascriptThrowInSetTimeout()
	{
		//_jintVirtualMachine.Engine.SetValue("testMethod", stringFormatDelegate);

		_jintVirtualMachine.EvalInMem("ThrowErrorInSetTimeout.js", @"
			// TPC: adding scope block here - since we declare/define counter in both tests (at global level)
			{
				let counter = 0;
				var callback = function(){
					++counter;
					console.log('js callback: counter: ' + counter);
					throw new Error('this is an awesome error');
					testMethod('neat');
				}

				setTimeout(callback, 10);
			}
		");

		//Console.WriteLine("JavascriptThrowInSetTimeout: after JVM.Eval: _asyncException: " + _asyncException);

		for (int i = 0; (i < 100) && (_asyncException == null); ++i)
		{
			System.Threading.Thread.Sleep(10);
		}

		//Console.WriteLine("JavascriptThrowInSetTimeout: after Thread.Sleep: _asyncException: " + _asyncException);
		Assert.IsInstanceOf<Jint.Runtime.JavaScriptException>(_asyncException);
	}


}
