using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Jint;
using Jint.Runtime;
using Jint.Native;
using Jint.Runtime.Interop;
using Jint.CommonJS;
using Javascript.Core;

namespace Javascript.Jint
{
	public class JavascriptInvalidFunctionNameException : System.Exception
	{
		public JavascriptInvalidFunctionNameException(string functionName)
			: base(string.Format("{0} contains invalid characters", functionName))
		{
		}
	}

	public class JavascriptFunctionParsingException : System.Exception
	{
		public JavascriptFunctionParsingException(string script, Exception ex)
		   : base(string.Format("Javascript Parsing Error:\n\n{0}\n\n{1}", JintVirtualMachine.AddLineNumbersToScript(script), ex.Message))
		{
		}
	}

	public class JintVirtualMachine : JavascriptVirtualMachine
	{
		private IJavascriptLogger _logger;

		// TPC: don't think we need this - we probably want to direct all calls thru the FileResolver
		private Core.IFileSystem _fileSystem;
		private Core.FileResolver _fileResolver;
		protected ModuleLoadingEngine _moduleLoadingEngine;

		private Core.InMemoryFileSystem _inMemFileSystem;
		protected ModuleLoadingEngine _inMemModuleLoadingEngine;

		private Engine _engine;

		public Engine Engine
		{
			get { return _engine; }
		}

		// We could also have an "unhandled exception handler" event in this class - but atm - we don't need to throw exceptions "out of band"
		public override event Javascript.Core.UnhandledExceptionEventHandler UnhandledException
		{
			add
			{
				if (value != null)
				{
					this._scheduler.UnhandledException += value;
				}
			}
			remove
			{
				this._scheduler.UnhandledException -= value;
			}
		}

		// TPC: TODO: could make the fileSystem be optional (i.e. default assign null) - in case users don't need/want a filesystem
		public JintVirtualMachine(IJavascriptLogger logger, IFileSystem fileSystem, IReadOnlyList<AssemblyNamespaces>? boundAssemblies = null,
			IJavascriptConsole? jintConsole = null, IReadOnlyList<Type>? extensionMethodsTypes = null)
		{
			_logger = logger;
			if (jintConsole == null)
			{
				jintConsole = new DefaultJavascriptConsole();
			}

			_fileSystem = fileSystem;
			var inMemFileSystem = _fileSystem as Core.InMemoryFileSystem;
			if (inMemFileSystem != null)
			{
				_inMemFileSystem = inMemFileSystem;
			}
			else
			{
				_inMemFileSystem = new Core.InMemoryFileSystem();
			}

			_fileResolver = new Javascript.Core.FileResolver(fileSystem);

			Options options = new Options();
			var boundAssembliesList = new System.Collections.Generic.List<System.Reflection.Assembly>();
			// TPC: the following call is very deceptive - we're registering the entire assembly that JintRunner is within - but this is only used for javascript like this:
			// var Foo = importNamespace('Foo');
			// That is - if you want to make all classes/symbols accessible within a particular namespace within a particular assembly
			//boundAssemblies.Add(typeof(JintRunner).Assembly);
			if (boundAssemblies != null)
			{
				foreach (var boundAssembly in boundAssemblies)
				{
					boundAssembliesList.Add(boundAssembly.Assembly);
				}
			}
			options.AllowClr(boundAssembliesList.ToArray());
			options.AllowOperatorOverloading();

			if (extensionMethodsTypes != null)
			{
				foreach (var type in extensionMethodsTypes)
				{
					//_logger.Debug("JintVirtualMachine: extensionMethodsTypes: type: " + type);
					options.AddExtensionMethods(type);
				}
			}

			// TPC: if we wanted module loading
			//c.Modules.Enabled = true;
			//c.WithModuleLoader(new global::Jint.Runtime.Modules.DefaultModuleLoader(null));

			_engine = new Engine(options);

			_engine.SetValue("console", jintConsole);

			_engine.SetValue("setTimeout", new Action<Action, int>((action, msec) =>
		   {
			   uint timerId = SetTimeout(action, msec);
			   // TPC: anytime you create a timer - something has to clean it up
		   }));

			_engine.SetValue("setInterval", new Func<Action, int, uint>((action, msec) =>
			{
				// TPC: anytime you create a timer - something has to clean it up
				uint timerId = SetInterval(action, msec);
				return timerId;
			}));

			_engine.SetValue("clearInterval", new Action<uint>((timerId) =>
			{
				ClearTimer(timerId);
			}));

			if (boundAssemblies != null)
			{
				foreach (var boundAssembly in boundAssemblies)
				{
					foreach (var namespaceName in boundAssembly.Namespaces)
					{
						NamespaceReference namespaceReference = new NamespaceReference(_engine, namespaceName);
						_engine.Realm.GlobalObject.Set(namespaceName, namespaceReference);
					}
				}
			}

			var commonJsPathResolver = new CommonJSPathResolver(new string[] { ".js" }, _fileSystem);
			_moduleLoadingEngine = new ModuleLoadingEngine(_engine, _fileSystem, commonJsPathResolver);

			if (inMemFileSystem != null)
			{
				_inMemModuleLoadingEngine = _moduleLoadingEngine;
			}
			else
			{
				var inMemCommonJsPathResolver = new CommonJSPathResolver(new string[] { ".js" }, _inMemFileSystem);
				_inMemModuleLoadingEngine = new ModuleLoadingEngine(_engine, _inMemFileSystem, inMemCommonJsPathResolver);
			}

			// TPC: this bizarreness is to allow us to flag a module as an internal module - which doesn't go thru the "wrap js in function with 4 args" business
			ModuleLoadingEngine.ModuleRequested += _inMemModuleLoadingEngine_ModuleRequested;
		}

		public override void Dispose()
		{
			// TPC: not sure if there is a more deterministic way to shutdown a Jint.Engine
			_engine = null!;
		}

		private void _inMemModuleLoadingEngine_ModuleRequested(object? sender, ModuleRequestedEventArgs e)
		{
			if (sender == _inMemModuleLoadingEngine)
			{
				var moduleId = e.ModuleId;
				var scriptBytes = _inMemFileSystem.ReadAllBytes(moduleId);
				var scriptStr = System.Text.Encoding.Default.GetString(scriptBytes);
				try
				{
					e.Exports = _engine.Evaluate(scriptStr);
				}
				catch (JintJsException) // TPC: we have to do this awkwardness because JavaScriptException.Location is private - it should be protected - so we have to wrap JavaScriptException's with our JintJsExceptions and "new" the LineNumber and Column
				{
					throw;
				}
				catch (JavaScriptException ex)
				{
					throw new JintJsException(moduleId, ex);
				}
				e.Evaluated = true;
			}
		}

		protected JsValue Require(string moduleId)
		{
			// TPC: the field _module is never assigned to - so it has been removed
			return _moduleLoadingEngine.Load(moduleId, null);
		}

		public JsValue Execute(string moduleName)
		{
			//Console.WriteLine("Execute: moduleName: " + moduleName);
			try
			{
				return _moduleLoadingEngine.RunMain(moduleName);
			}
			catch (JintJsException) // TPC: we have to do this awkwardness because JavaScriptException.Location is private - it should be protected - so we have to wrap JavaScriptException's with our JintJsExceptions and "new" the LineNumber and Column
			{
				throw;
			}
			catch (JavaScriptException ex)
			{
				throw new JintJsException(moduleName, ex);
			}
		}

		public override object Eval(string moduleName)
		{
			return Execute(moduleName);
		}

		public bool IsModuleRegistered(string moduleName)
		{
			return _inMemModuleLoadingEngine.IsModuleRegistered(moduleName);
		}

		private JsValue ExecuteInMem(string moduleName)
		{
			//_logger.Debug("ExecuteInMem: moduleName: " + moduleName);
			try
			{
				var jsValue = _inMemModuleLoadingEngine.RunMain(moduleName);
				return jsValue;
			}
			catch (JintJsException) // TPC: we have to do this awkwardness because JavaScriptException.Location is private - it should be protected - so we have to wrap JavaScriptException's with our JintJsExceptions and "new" the LineNumber and Column
			{
				throw;
			}
			catch (JavaScriptException ex)
			{
				_logger.Debug("StackTrace: " + ex.StackTrace);
				throw new JintJsException(moduleName, ex);
			}
		}

		// TPC: DuktapeVirtualMachine exposes the following to typescript - JINT may want something like this if users plan on putting multiple roots for their javascript
		/*public void AddSearchPath(string path)
        {
            _fileResolver.AddSearchPath(path);
        }
        */

		private string EnsureExtension(string filename)
		{
			return filename != null && filename.EndsWith(".js") ? filename : filename + ".js";
		}

		public override void EvalInMem(string moduleName, string code)
		{
			// TPC: if we don't need/want to register InternalModule for this code - we could just dispatch this code to the engine directly
			//_engine.Evaluate(code);

			_inMemFileSystem.WriteFile(moduleName, code);
			ExecuteInMem(moduleName);
		}

		public const string FunctionPrefix = "function ";
		public const string SignaturePrefix = "( ";
		public const string SourcePrefix = " ) {\n";
		public const string SourcePostfix = "\n}";
		public static readonly int PrefixPostfixSize = FunctionPrefix.Length + SignaturePrefix.Length + SourcePrefix.Length + SourcePostfix.Length;

		public override string ScriptFromFunction(string functionName, string sourceCode, string parameterSignature = "")
		{
			var size = PrefixPostfixSize + functionName.Length + sourceCode.Length + parameterSignature.Length;

			var sb = new System.Text.StringBuilder(size, size);
			sb.Append(FunctionPrefix);
			sb.Append(functionName);
			sb.Append(SignaturePrefix);
			sb.Append(parameterSignature);
			sb.Append(SourcePrefix);
			sb.Append(sourceCode);
			sb.Append(SourcePostfix);

			var script = sb.ToString();
			return script;
		}

		public override bool RegisterFunction(string functionName, string sourceCode, string parameterSignature = "")
		{
			var valid = JintVirtualMachine.IsValidFunctionName(functionName);
			if (valid == false)
			{
				throw new JavascriptInvalidFunctionNameException(functionName);
			}

			if (this.IsModuleRegistered(functionName) == true)
			{
				return false;
			}

			var script = ScriptFromFunction(functionName, sourceCode, parameterSignature);
			try
			{
				this.EvalInMem(functionName, script);
				return true;
			}
			catch (Exception ex)
			{
				throw new JavascriptFunctionParsingException(script, ex);
			}
		}

		public override object InvokeFunction(string functionName, params object?[] parameters)
		{
			return this.Invoke(functionName, parameters);
		}
		public override object RegisterAndInvokeFunction(string functionName, string sourceCode, string parameterSignature = "", params object?[] parameters)
		{
			this.RegisterFunction(functionName, sourceCode, parameterSignature);
			try
			{
				return this.InvokeFunction(functionName, parameters);
			}
			catch (JavaScriptException ex)
			{
				var script = ScriptFromFunction(functionName, sourceCode, parameterSignature);
				var lines = script.Split("\n");

				var sb = new System.Text.StringBuilder("\n");
				for (var i = 0; i < lines.Length; ++i)
				{
					sb.Append(i + 1);
					sb.Append(": ");
					sb.AppendLine(lines[i]);
				}
				throw new Exception(sb.ToString(), ex);
			}
		}

		public override bool IsBool(object obj)
		{
			return obj is JsBoolean;
		}

		public override bool AsBool(object obj)
		{
			if (obj is JsBoolean v)
			{
				return v == JsBoolean.True;
			}

			throw new InvalidCastException("obj is not a JsBoolean");
		}
		public override bool TryAsBool(object obj, out bool value)
		{
			if (obj is JsBoolean v)
			{
				value = v == JsBoolean.True;
				return true;
			}

			value = false;
			return false;
		}

		public override bool IsNumber(object obj)
		{
			return obj is JsNumber;
		}
		public override double AsNumber(object obj)
		{
			if (obj is JsNumber v)
			{
				return (double)v.ToObject();
			}

			throw new InvalidCastException("obj is not a JsNumber");
		}
		public override bool TryAsNumber(object obj, out double value)
		{
			if (obj is JsNumber v)
			{
				value = (double)v.ToObject();
				return true;
			}

			value = 0;
			return false;
		}

		public override bool IsStruct<T>(object obj) where T : struct
		{
			if (obj is IObjectWrapper v)
			{
				return v.Target is T;
			}

			return false;
		}
		public override T AsStruct<T>(object obj) where T : struct
		{
			if (obj is IObjectWrapper v)
			{
				var target = v.Target;
				if (target is T t)
				{
					return t;
				}
			}

			throw new InvalidCastException("obj is not a Jint.Runtime.Interop.IObjectWrapper");
		}
		public override bool TryAsStruct<T>(object obj, out T value) where T : struct
		{
			if (obj is IObjectWrapper v)
			{
				var target = v.Target;
				if (target is T t)
				{
					value = t;
					return true;
				}
			}

			value = default(T);
			return false;
		}

		public override bool IsClass<T>(object obj) where T : class
		{
			if (obj is IObjectWrapper v)
			{
				return v.Target is T;
			}

			return false;
		}
		public override T AsClass<T>(object obj) where T : class
		{
			if (obj is IObjectWrapper v)
			{
				var target = v.Target;
				if (target is T t)
				{
					return t;
				}
			}

			throw new InvalidCastException("obj is not a Jint.Runtime.Interop.IObjectWrapper");
		}
		public override bool TryAsClass<T>(object obj, out T? value) where T : class
		{
			if (obj is IObjectWrapper v)
			{
				var target = v.Target;
				if (target is T t)
				{
					value = t;
					return true;
				}
			}

			value = default(T);
			return false;
		}

		public JsValue Invoke(string propertyName, params object?[] arguments)
		{
			return _engine.Invoke(propertyName, arguments);
		}

		public void SetValue(string name, Delegate value)
		{
			_engine.SetValue(name, value);
		}

		public override void SetValue(string name, Type type)
		{
			_engine.SetValue(name, type);
		}

		public override void SetValue(string name, Object obj)
		{
			_engine.SetValue(name, obj);
		}

		public override void Update(int elapsedMs)
		{
			UpdateScheduler(elapsedMs);
		}

		public static string JavascriptInvalidVariableRegexPattern = @"^[^a-zA-Z_$]|[^\w$]";

		public static bool IsValidFunctionName(string functionName)
		{
			var invalid = Regex.IsMatch(functionName, JavascriptInvalidVariableRegexPattern);
			return invalid == false;
		}

		public static string NormalizeFunctionName(string functionName)
		{
			var normalized = Regex.Replace(functionName, JavascriptInvalidVariableRegexPattern, "_");
			return normalized;
		}

		public static string AddLineNumbersToScript(string script)
		{
			var lines = script.Split("\n");

			var sb = new System.Text.StringBuilder(script.Length + lines.Length * 2);
			for (var i = 0; i < lines.Length; ++i)
			{
				sb.AppendFormat("{0}: {1}\n", i + 1, lines[i]);
			}

			return sb.ToString();
		}
	}

	// TPC: because the Jint.Runtime.JavaScriptException has a private setter on its Location
	// public Location Location { get; private set; }
	// we can't properly subclass it - otherwise the data pulled from Location (in generation of callstack, error, etc) will be incorrect

	// TPC: for reference - Jint.Runtime.JavaScriptException extends Jint.Runtime.JintException
	public class JintJsException : JavaScriptException
	{
		private string _moduleName;

		public string ModuleName
		{
			get { return _moduleName; }
		}

		private JavaScriptException _jsException;

		// we don't propagate the constructor call to our base class because it largely relies on a ErrorConstructor which generates a JsValue for Error
		public JintJsException(string moduleName, JavaScriptException jsException) : base(jsException.Error)
		{
			_moduleName = moduleName;
			_jsException = jsException;
		}

		public override string Message
		{
			get
			{
				var scriptFilename = (_moduleName != null) ? "Filepath: " + _moduleName + " " : "";
				var errorMsg = $"{scriptFilename}{_jsException.Message}";
				return errorMsg;
			}
		}

		public override string? StackTrace
		{
			get { return _jsException.StackTrace; }
		}

		public int LineNumber => _jsException.Location.Start.Line;

		public int Column => _jsException.Location.Start.Column;
	}
}
