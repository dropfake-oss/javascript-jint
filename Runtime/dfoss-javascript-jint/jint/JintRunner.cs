using System;
using System.Collections.Generic;
using Javascript.Core;

namespace Javascript.Jint
{
	public class JintRunner : IJavascriptRunner
	{
		private Dictionary<string, JintVirtualMachine> _jintVms = new Dictionary<string, JintVirtualMachine>();

		private IJavascriptLogger _logger;
		private IFileSystem _fileSystem;
		private IReadOnlyList<AssemblyNamespaces>? _boundAssemblies = null;
		private IJavascriptConsole? _jintConsole = null;
		private IReadOnlyList<Type>? _extensionMethodsTypes = null;
		private IReadOnlyList<Type>? _reflectedTypes;

		public JintRunner(
			IJavascriptLogger logger,
			IFileSystem fileSystem,
			IReadOnlyList<AssemblyNamespaces>? boundAssemblies = null,
			IJavascriptConsole? jintConsole = null,
			IReadOnlyList<System.Type>? reflectedTypes = null,
			IReadOnlyList<System.Type>? extensionMethodsTypes = null)
		{
			_logger = logger;
			_fileSystem = fileSystem;
			_boundAssemblies = boundAssemblies;
			_jintConsole = jintConsole;
			_reflectedTypes = reflectedTypes;
			_extensionMethodsTypes = extensionMethodsTypes;
		}

		public void Dispose()
		{
			List<string> jintVmIds = new List<string>();
			foreach (var jintVmEntry in _jintVms)
			{
				jintVmIds.Add(jintVmEntry.Key);
				var jintVm = jintVmEntry.Value;
				jintVm.Dispose();
			}
			foreach (var jintVmId in jintVmIds)
			{
				_jintVms.Remove(jintVmId);
			}
		}

		public void BindClassesToJint(IJavascriptVirtualMachine jsVm)
		{
			if (_reflectedTypes != null)
			{
				for (int i = 0; i < _reflectedTypes.Count; ++i)
				{
					var type = _reflectedTypes[i];
					jsVm.SetValue(type.FullName ?? type.Name, type);
				}
			}
		}

		public IJavascriptVirtualMachine CreateJavascriptVirtualMachine(string id)
		{
			var jintVm = new JintVirtualMachine(_logger, _fileSystem, _boundAssemblies, _jintConsole, _extensionMethodsTypes);
			BindClassesToJint(jintVm);
			_jintVms[id] = jintVm;
			return jintVm;
		}

		public bool RemoveJavascriptVirtualMachine(string id)
		{
			return _jintVms.Remove(id);
		}

		public IJavascriptVirtualMachine? GetJavascriptVirtualMachine(string id)
		{
			if (_jintVms.TryGetValue(id, out JintVirtualMachine? vm) == true)
			{
				return vm;
			}

			return null;
		}

		public IJavascriptVirtualMachine GetOrCreateJavascriptVirtualMachine(string id)
		{
			var vm = this.GetJavascriptVirtualMachine(id);
			if (vm == null)
			{
				vm = this.CreateJavascriptVirtualMachine(id);
			}
			return vm;
		}

		public void Update(int elapsedMs)
		{
			foreach (var jintVmEntry in _jintVms)
			{
				var jintVm = jintVmEntry.Value;
				jintVm.Update(elapsedMs);
			}
		}
	}
}
