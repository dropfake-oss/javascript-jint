
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Jint.Native;
using Jint.Native.Function;
using Jint.Native.Object;
using Jint.Runtime;
using Jint.Runtime.Interop;

namespace Jint.CommonJS
{
	public class Module : IModule
	{
		/// <summary>
		/// This module's children
		/// </summary>
		public List<IModule> Children { get; } = new List<IModule>();

		protected Module? parentModule;

		protected ModuleLoadingEngine engine;

		/// <summary>
		/// Determines if this module is the main module.
		/// </summary>
		public bool isMainModule => this.parentModule == null;

		public string Id { get; set; }

		/// <summary>
		/// Contains the module's public API.
		/// </summary>
		public JsValue Exports { get; set; }

		public Module? ParentModule
		{
			get
			{
				return parentModule;
			}
		}

		public readonly string FilePath;

		/// <summary>
		/// Creates a new Module instaznce with the specified module id. The module is resolved to a file on disk
		/// according to the CommonJS specification.
		/// </summary>
		internal Module(ModuleLoadingEngine e, string moduleId, Module? parent = null)
		{
			if (e == null)
			{
				throw new System.ArgumentNullException(nameof(e));
			}

			this.engine = e;

			if (string.IsNullOrEmpty(moduleId))
			{
				throw new System.ArgumentException("A moduleId is required.", nameof(moduleId));
			}

			Id = moduleId;
			this.FilePath = e.Resolver.ResolvePath(Id, parent ?? this);
			this.parentModule = parent;

			if (parent != null)
			{
				parent.Children.Add(this);
			}

			//this.Exports = engine.engine.Object.Construct(new JsValue[] { });
			this.Exports = engine.engine.Realm.Intrinsics.Object.Construct(new JsValue[] { });

			string extension = Path.GetExtension(this.FilePath);
			// We need the following in the scenario of a javascript require(..) where we don't append the file extension
			// If you think the following is not necessary - then just uncomment out the "throw new System.Exception.." - in the "if" below
			if (string.IsNullOrEmpty(extension))
			{
				extension = ".js";
				//throw new System.Exception("not sure if there is a way around assuming a .js file extension on: " + this.filePath);
			}
			var loader = this.engine.FileExtensionParsers[extension] ?? this.engine.FileExtensionParsers["default"];

			e.ModuleCache.Add(Id, this);

			loader(this.FilePath, this);
		}

		protected JsValue Require(string moduleId)
		{
			return engine.Load(moduleId, this);
		}

		public JsValue Compile(string sourceCode, string filePath)
		{
			var moduleObject = JsValue.FromObject(this.engine.engine, this);

			const string signature = "module, exports, __dirname, require";

			var module = JsValue.FromObject(this.engine.engine, this);
			var __dirname = Path.GetDirectoryName(filePath);
			if (__dirname == null)
			{
				throw new System.NullReferenceException("Module: Compile: path returned from Path.GetDirectoryName(filePath) returned null for filePath: " + filePath);
			}
			var exports = this.Exports;
			var require = new ClrFunctionInstance(this.engine.engine, filePath, (thisObj, arguments) => Require(arguments.At(0).AsString()));

			ExecuteInScope.ExecuteScript(
				engine.engine,
				sourceCode,
				signature,
				module,
				exports,
				__dirname,
				require);

			return Exports;
		}
	}

}
