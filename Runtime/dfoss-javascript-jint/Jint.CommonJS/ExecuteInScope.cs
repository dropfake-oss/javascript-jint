
namespace Jint.CommonJS
{
	public static class ExecuteInScope
	{
		public const string SignaturePrefix = ";(function (";
		public const string SignaturePostfix = ") {\n";
		public const string SourcePostfix = "\n})";
		public static readonly int PrefixPostfixSize = SignaturePrefix.Length + SignaturePostfix.Length + SourcePostfix.Length;

		public static void ExecuteScript(Jint.Engine engine, string sourceCode, string parameterSignature, params object[] arguments)
		{
			var size = PrefixPostfixSize + sourceCode.Length + parameterSignature.Length;

			var sb = new System.Text.StringBuilder(size, size);
			sb.Append(SignaturePrefix);
			sb.Append(parameterSignature);
			sb.Append(SignaturePostfix);
			sb.Append(sourceCode);
			sb.Append(SourcePostfix);

			var script = sb.ToString();
			var wrapped = engine.Evaluate(script);
			engine.Invoke(wrapped, arguments);
		}

		public static void ExecuteScript(Jint.Engine engine, string sourceCode, string[] parameters, params object[] arguments)
		{
			var size = PrefixPostfixSize + sourceCode.Length;
			var paramsLength = parameters.Length;
			if (paramsLength > 0)
			{
				//Add the parameters and add the required ","
				foreach (var param in parameters)
				{
					size += param.Length;
				}
				size += (paramsLength - 1);
			}

			var sb = new System.Text.StringBuilder(size, size);
			sb.Append(SignaturePrefix);
			if (parameters.Length > 0)
			{
				for (var i = 0; i < paramsLength;)
				{
					sb.Append(parameters[i]);
					i++;
					if (i < paramsLength)
					{
						sb.Append(",");
					}
				}
			}
			sb.Append(SignaturePostfix);
			sb.Append(sourceCode);
			sb.Append(SourcePostfix);

			var script = sb.ToString();
			var wrapped = engine.Evaluate(script);
			engine.Invoke(wrapped, arguments);
		}

		public struct Parameter
		{
			public string Param;
			public object Value;
		}

		public static void ExecuteScript(Jint.Engine engine, string sourceCode, Parameter[] parameters)
		{
			var arguments = new object[parameters.Length];

			var size = PrefixPostfixSize + sourceCode.Length;
			var paramsLength = parameters.Length;
			if (paramsLength > 0)
			{
				//Add the parameters and add the required ","
				for (var i = 0; i < paramsLength; ++i)
				{
					var param = parameters[i];
					size += param.Param.Length;
					arguments[i] = param.Value;
				}
				size += (paramsLength - 1);
			}

			var sb = new System.Text.StringBuilder(size, size);
			sb.Append(SignaturePrefix);
			if (parameters.Length > 0)
			{
				for (var i = 0; i < paramsLength;)
				{
					var param = parameters[i];
					sb.Append(param.Param);
					i++;
					if (i < paramsLength)
					{
						sb.Append(",");
					}
				}
			}
			sb.Append(SignaturePostfix);
			sb.Append(sourceCode);
			sb.Append(SourcePostfix);

			var script = sb.ToString();
			var wrapped = engine.Evaluate(script);
			engine.Invoke(wrapped, arguments);
		}
	}
}
