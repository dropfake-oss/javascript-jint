using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Jint.CommonJS
{
	public class CommonJSPathResolver : IModuleResolver
	{
		private readonly IEnumerable<string> extensionHandlers;

		private Javascript.Core.IFileSystem? _fs;

		public CommonJSPathResolver(IEnumerable<string> extensionHandlers, Javascript.Core.IFileSystem? fs = null)
		{
			_fs = fs;
			this.extensionHandlers = extensionHandlers;
		}

		public string ResolvePath(string moduleId, Module? parent)
		{
			var inMemFs = _fs as Javascript.Core.InMemoryFileSystem;
			if (inMemFs != null)
			{
				if (!inMemFs.Exists(moduleId))
				{
					throw new FileNotFoundException($"CommonJSPathResolver: ResolvePath: Module {moduleId} could not be resolved using Filesystem: " + inMemFs);
				}

				return moduleId;
			}

			bool isExternalModule = !moduleId.StartsWith(".") && parent?.FilePath != null;

			// TPC: we can't use a global like Environment.CurrentDirectory because that won't work in contexts/platforms like Unity
			string cwd = string.Empty;
			if (parent?.FilePath != null)
			{
				var currentWorkingDir = Path.GetDirectoryName(parent.FilePath);
				if (currentWorkingDir == null)
				{
					throw new System.NullReferenceException("CommonJSPathResolver: ResolvePath: Path.GetDirectoryName(parent.FilePath) returned null for parent.FilePath: " + parent.FilePath);
				}
				cwd = currentWorkingDir;
			}
			else if (Path.IsPathRooted(moduleId))
			{
				cwd = moduleId;
			}
			else // TPC: IFileSystem shouldn't need absolute file paths
			{
				//throw new System.Exception("old code wanted us to use this: " + Environment.CurrentDirectory);
			}

			if (isExternalModule && parent?.ParentModule != null)
			{
				// for external modules we look in the parent directory of the main module
				var rootModule = parent.ParentModule;

				while (rootModule.ParentModule != null)
				{
					rootModule = rootModule.ParentModule;
				}

				var parentDirInfo = new DirectoryInfo(rootModule.Id).Parent;
				if (parentDirInfo == null)
				{
					throw new System.NullReferenceException("CommonJSPathResolver: ResolvePath: DirectoryInfo(rootModule.Id).Parent returned null for rootModule.Id: " + rootModule.Id);
				}
				var currentWorkingDir = parentDirInfo.FullName;
				cwd = currentWorkingDir;
			}

			var path = Path.Combine(cwd, moduleId);

			if (_fs != null)
			{
				if (!_fs.Exists(path)) // TPC: this is likely also necessary for how we require js files without using file extension
				{
					var updatedPath = path + ".js"; // not sure we want to iterate over extensionHandlers
					if (!_fs.Exists(updatedPath))
					{
						throw new FileNotFoundException($"Module {path} could not be resolved using Filesystem: " + _fs);
					}
					return updatedPath;
				}

				return path;
			}

			/*
			* - Try direct file in case an extension is provided
			* - if directory, return directory/index
			*/

			if (Directory.Exists(path))
			{
				path = Path.Combine(path, "index");
			}

			if (!File.Exists(path))
			{
				foreach (var tryExtension in extensionHandlers.Where(i => i != "default"))
				{
					string innerCandidate = path + tryExtension;
					if (File.Exists(innerCandidate))
					{
						return innerCandidate;
					}
				}

				throw new FileNotFoundException($"Module {path} could not be resolved.");
			}

			return path;
		}
	}
}
