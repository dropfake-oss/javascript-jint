
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Jint.Native;
using Jint.Native.Object;
using Jint.Runtime.Interop;

namespace Jint.CommonJS
{

	public class ModuleLoadingEngine
	{
		public static event EventHandler<ModuleRequestedEventArgs>? ModuleRequested; // TPC: setting an event to null! expliclty is odd

		public delegate JsValue FileExtensionParser(string path, IModule module);

		public Dictionary<string, IModule> ModuleCache = new Dictionary<string, IModule>();
		public Dictionary<string, FileExtensionParser> FileExtensionParsers = new Dictionary<string, FileExtensionParser>();

		public readonly Engine engine;
		public IModuleResolver Resolver { get; set; }

		private Javascript.Core.IFileSystem _fs;

		// The JINT doc's indicate the correct way to do JSON Parsing is to locally instantiate an instance of their JsonParser (with an instance of Engine) and off you go - hence the following member
		Jint.Native.Json.JsonParser _jsonParser;

		public ModuleLoadingEngine(Engine e, Javascript.Core.IFileSystem fs, IModuleResolver? resolver = null)
		{
			_fs = fs;
			this.engine = e;
			if (resolver != null)
			{
				this.Resolver = resolver;
			}
			else
			{
				this.Resolver = new CommonJSPathResolver(this.FileExtensionParsers.Keys);
			}

			FileExtensionParsers.Add("default", this.LoadJS);
			FileExtensionParsers.Add(".js", this.LoadJS);
			FileExtensionParsers.Add(".json", this.LoadJson);

			_jsonParser = new Jint.Native.Json.JsonParser(engine);
		}

		private string ReadFile(string path)
		{
			string sourceCode;
			if (_fs != null)
			{
				var fileBytes = _fs.ReadAllBytes(path);
				sourceCode = System.Text.Encoding.Default.GetString(fileBytes);
			}
			else
			{
				sourceCode = File.ReadAllText(path);
			}
			return sourceCode;
		}

		private JsValue LoadJS(string path, IModule module)
		{
			if (module == null)
			{
				throw new System.NullReferenceException("ModuleLoadingEngine: LoadJS: we need a non null module!");
			}
			var sourceCode = ReadFile(path);
			var mundaneModule = (module as Module);
			if (mundaneModule != null)
			{
				module.Exports = mundaneModule.Compile(sourceCode, path);
			}
			else
			{
				module.Exports = engine.Evaluate(sourceCode);
			}
			return module.Exports;
		}

		private JsValue LoadJson(string path, IModule module)
		{
			var sourceCode = ReadFile(path);
			// TPC: there is an overload of JsonParser.Parser(..) that takes Esprima.ParserOptions
			_jsonParser.Parse(sourceCode);
			return module.Exports;
		}

		protected ModuleLoadingEngine RegisterInternalModule(InternalModule mod)
		{
			ModuleCache.Add(mod.Id, mod);
			return this;
		}

		/// <summary>
		/// Registers an internal module to the provided delegate handler.
		/// </summary>
		public ModuleLoadingEngine RegisterInternalModule(string id, Delegate d)
		{
			this.RegisterInternalModule(id, new DelegateWrapper(engine, d));
			return this;
		}

		/// <summary>
		/// Registers an internal module under the specified id to the provided .NET CLR type.
		/// </summary>
		public ModuleLoadingEngine RegisterInternalModule(string id, Type clrType)
		{
			this.RegisterInternalModule(id, TypeReference.CreateTypeReference(engine, clrType));
			return this;
		}

		/// <summary>
		/// Registers an internal module under the specified id to any JsValue instance.
		/// </summary>
		public ModuleLoadingEngine RegisterInternalModule(string id, JsValue value)
		{
			this.RegisterInternalModule(new InternalModule(id, value));
			return this;
		}

		/// <summary>
		/// Registers an internal module to the specified ID to the provided object instance.
		/// </summary>
		public ModuleLoadingEngine RegisterInternalModule(string id, object instance)
		{
			this.RegisterInternalModule(id, JsValue.FromObject(this.engine, instance));
			return this;
		}

		public bool IsModuleRegistered(string moduleName)
		{
			var registered = ModuleCache.ContainsKey(moduleName);
			return registered;
		}

		public IModule? GetModuleRegistered(string moduleName)
		{
			ModuleCache.TryGetValue(moduleName, out IModule? module);
			return module;
		}

		public JsValue RunMain(string mainModuleName)
		{
			if (string.IsNullOrWhiteSpace(mainModuleName))
			{
				throw new System.ArgumentException("A Main module path is required.", nameof(mainModuleName));
			}

			return this.Load(mainModuleName);
		}

		public JsValue Load(string moduleName, Module? parent = null)
		{
			IModule mod;

			if (string.IsNullOrEmpty(moduleName))
			{
				throw new System.ArgumentException("moduleName is required.", nameof(moduleName));
			}

			if (ModuleCache.ContainsKey(moduleName))
			{
				mod = ModuleCache[moduleName];
				if (parent != null)
				{
					parent.Children.Add(mod);
				}
				return mod.Exports;
			}

			var requestedModule = new ModuleRequestedEventArgs(moduleName);
			ModuleRequested?.Invoke(this, requestedModule);
			// TPC: the following check is ambiguous - it is possible for a script to evaluate successfully - but still return null or JsValue.Undefined
			//if (requestedModule.Exports != null && requestedModule.Exports != JsValue.Undefined)
			if (requestedModule.Evaluated)
			{
				var internalModExports = requestedModule.Exports;
				if (internalModExports == null)
				{
					internalModExports = engine.Realm.Intrinsics.Object.Construct(new JsValue[] { });
				}
				ModuleCache.Add(moduleName, mod = new InternalModule(moduleName, internalModExports));
				return mod.Exports;
			}

			return new Module(this, moduleName, parent).Exports;
		}
	}
}
