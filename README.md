# Javascript.Jint

This package is a thin wrapper on the package:
https://github.com/sebastienros/jint

This package provides most of the fundamental functionality found in javascript - like setTimeout(..), setInterval(..), clearInterval(..) etc by using the filesystem, scheduling/timer functionality found in this package:
https://gitlab.com/dropfake-oss/javascript-core

# JINT

Source repository:
https://github.com/sebastienros/jint

Occasionally our group will have features/functionality that aren't integrated into mainline JINT - so still in our fork: 
https://github.com/source-transformer/jint

When there is a need for an updated version of the Jint.dll - ensure you grab the Jint.dll from the netstandard2.1 directory.
# Expose/Reflect Individual C# Classes or Provide Access to Entire Namespace

You can bind a C# class (type) to JINT by doing something like the following:

```
JintVirtualMachine.SetValue("GameApi", typeof(GameApi));
```

However, if you want to give JINT access to an entire namespace inside of an Assembly - you can do this (options are handed to the Engine constructor):

```
options.AllowClr(<Assembly>);
```

If you attempt to use importNamspace(..) without calling:

```
options.AllowClr();
var engine = new Engine(options);
```

You'll get the following error:

```
Jint.Runtime.JavaScriptException : importNamespace is not defined
```

And if you attempt to importNamespace from an Assembly that you haven't explicitly passed into AllowClr(..) - then you'll get the following error (for example if you attempt to instantiate a class from that assembly):

```
Jint.Runtime.JavaScriptException : <namespace path>.<class name> is not a constructor
```

## Extension Methods

If your types/classes have extension methods - you'll need to pass those types/classes into JintVirtualMachine constructor.
## Module Loading

To get module loading working - you'll need to build JINT yourself - instead of grabbing it from the NUGET.

Specifically - you'll need to install .Net 6.*:

```
Microsoft.NETCore.App 6.0.1 [/usr/local/share/dotnet/shared/Microsoft.NETCore.App]
```

and then build the solution using the command line (since visual studio community doesn't know how to use .Net 6.*):

```
dotnet build ./Jint.sln
```

### Integration

https://pleasenophp.github.io/posts/using-real-javascript-with-unity.html#1-create-project

C# 10 adds record structs and these are used in JINT - so that means you need .NET 6.

Trying to compile the source locally on a mac doesn't seem to work - the errors seem to imply that we're using an older version of .NET than required.  There is a very small bit of the visual studio community UI that shows "Unsupported .NET installation detected".

If you look here: 
https://docs.microsoft.com/en-us/visualstudio/mac/net-core-support?view=vsmac-2019

it looks like the top version of .NET that visual studio community can use is .NET 5.  So you might be able to get around this by getting "Visual Studio Preview" on your mac:

https://visualstudio.microsoft.com/vs/mac/preview/

This does uninstall (and hopefully reinstall) .NET though... sooo... use at your own risk.

Altnernatively - we can get the prebuilt binaries:
https://www.nuget.org/packages/Jint/

It's nice to set up something at the command-line - here is how you can do it:

This is the version from several years ago:
```
nuget install Jint -OutputDirectory /Users/tcassidy/github/jint_test/packages -Version 2.11.58
```

This is a more up to date version:
# JINT

source repository:
https://github.com/sebastienros/jint

Dependencies

dfoss-javascript-jint depends on JINT + dfoss-javascript-core.
## JINT in Unity

If you've never used JINT before - the following document does a very good/thorough job of how to integrate vanialla JINT into a Unity project:
https://pleasenophp.github.io/posts/using-real-javascript-with-unity.html

### Integration

https://pleasenophp.github.io/posts/using-real-javascript-with-unity.html#1-create-project

C# 10 adds record structs and these are used in JINT - so that means you need .NET 6.

Trying to compile the source locally on a mac doesn't seem to work - the errors seem to imply that we're using an older version of .NET than required.  There is a very small bit of the visual studio community UI that shows "Unsupported .NET installation detected".

If you look here: 
https://docs.microsoft.com/en-us/visualstudio/mac/net-core-support?view=vsmac-2019

it looks like the top version of .NET that visual studio community can use is .NET 5.  So you might be able to get around this by getting "Visual Studio Preview" on your mac:

https://visualstudio.microsoft.com/vs/mac/preview/

This does uninstall (and hopefully reinstall) .NET though... sooo... use at your own risk.


Altnernatively - we can get the prebuilt binaries:
https://www.nuget.org/packages/Jint/

It's nice to set up something at the command-line - here is how you can do it:

This version is the non-beta from several years ago:

```
nuget install Jint -OutputDirectory /Users/tcassidy/github/jint_test/packages -Version 2.11.58
```

and this is the latest version (at the time of this document being created):

```
nuget install Jint -OutputDirectory /Users/tcassidy/github/jint_test/packages -Version 3.0.0-beta-2037
```

DropFake's API Compatibility in Unity is set to .NET Standard 2.1.

So you'll want to copy the Jint.dll from the "netstandard2.0" directory.

## Contributions

Just create a merge request and tag anyone that you'd like to review your update.
